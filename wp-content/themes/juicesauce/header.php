<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Juicesauce
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css"  media="all"/> -->

<?php wp_head(); ?>


  <style media="screen">
    .row {
      display: block;
    }
    html {
      /*margin-top: 0 !important;*/
    }

    .nav-link {
      padding: initial;
    }
  </style>

  <meta name="google-site-verification" content="rxVkZt4yTW0GW7kaTNYI6lbfxVhEVtAiDQtf10tEbGc" />
  </head>

  <script type="text/javascript">
  // $(window).on('load', function() { // makes sure the whole site is loaded
  // $('#status').fadeOut(); // will first fade out the loading animation
  // $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
  // $('body').delay(150).css({'overflow':'visible'});
  //


  // })


  document.addEventListener("DOMContentLoaded", function(){
  	$('.preloader-background').delay(1200).fadeOut('slow');

  	$('.preloader-wrapper')
  		.delay(1200)
  		.fadeOut();
  });

  </script>


  <style media="screen">


  /* Preloader */

  #preloader {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #fff;
  opacity: .9;
  /* change if the mask should have another color then white */
  z-index: 99;
  /* makes sure it stays on top */
  }

  #status {
  width: 200px;
  height: 200px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  /* centers the loading animation vertically one the screen */
  background-image: url(/wp-content/themes/juicesauce/img/status.gif);
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
  }


  .preloader-background {
  	display: flex;
  	align-items: center;
  	justify-content: center;
  	background-color: #eee;

  	position: fixed;
  	z-index: 100;
  	top: 0;
  	left: 0;
  	right: 0;
  	bottom: 0;
  }

  </style>

  <!-- Preloader -->
  <!-- <div id="preloader">
    <div id="status">&nbsp;</div>
  </div> -->


  <div class="preloader-background">
  	<div class="preloader-wrapper big active">
  		<div class="spinner-layer spinner-blue-only">
  			<div class="circle-clipper left">
  				<div class="circle"></div>
  			</div>
  			<div class="gap-patch">
  				<div class="circle"></div>
  			</div>
  			<div class="circle-clipper right">
  				<div class="circle"></div>
  			</div>
  		</div>
  	</div>
  </div>



<body <?php body_class(); ?>>



<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'juicesauce' ); ?></a>

	<header id="masthead" class="site-header" role="banner">


		<nav id="site-navigation" class="main-navigation white-text light-blue darken-1 z-depth-0" role="navigation">
	    <div class="nav-wrapper">
	      <a class="brand-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
	      <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
	      <ul class="right hide-on-med-and-down">
					<li class="dropdown-button" data-constrainwidth="false" data-activates='dropdown2'><a href="/"><i class="fa fa-list-ul" aria-hidden="true"></i> Recipes</a>

          </li>
          <ul id="dropdown2" class="dropdown-content" data-constrainwidth="false">
            <li><a href="/add-recipe"><i class="fa fa-plus" aria-hidden="true"></i> Add Recipe</a></li>
          </ul>
          <li><a href="/flavors"><i class="fa fa-eyedropper" aria-hidden="true"></i> Flavors</a></li>
          <li><a href="/members"><i class="fa fa-users" aria-hidden="true"></i> Mixologists</a></li>
          <li><a href="/contact"><i class="fa fa-envelope" aria-hidden="true"></i> Contact</a></li>
          <?php if ( !is_user_logged_in() ) { echo '<li><a href="/login">Login</a></li>'; } ?>
          <!-- <?php if ( !is_user_logged_in() ) { echo '<li><a href="/register">Register</a></li>'; } ?> -->
          <?php $current_user = wp_get_current_user(); ?>
            <?php if ( is_user_logged_in() ) { ?>
          <?php if ( ($current_user instanceof WP_User) ) { ?>
            <li class="account"><a class='dropdown-button' href='/account' data-activates='dropdown1'><?php echo get_avatar( $current_user->user_email, 32 ); ?><i class="material-icons right">arrow_drop_down</i><?php um_fetch_user($current_user->ID);
 echo um_user('display_name'); ?></a>
            </li>

              <?php } } ?>

            <?php if ( is_user_logged_in() ) { ?>
              <ul id='dropdown1' class='dropdown-content' data-constrainwidth="false">
                    <li><a href="/account">Account</a></li>
                    <li><a href="/user">View Profile</a></li>
                    <li><a href="/logout">Logout</a></li>
                  </ul>
                    <?php } ?>


	      </ul>
        <ul id="slide-out" class="side-nav full">
          <li><a href="/">Recipes</a></li>
          <li><a href="/add-recipe">Add Recipe</a></li>
        </ul>


	    </div>
	  </nav>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
