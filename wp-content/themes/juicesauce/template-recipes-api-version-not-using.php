<?php
/**
 * Template Name: Recipes List (API VERSION)
 *
 *
 * @package understrap
 */


get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<style media="screen">
	.recipe-card .card-action .btn-flat {
		font-size: 12px;
		padding: 0 1rem;
		margin-right: 6px !important;
		height: initial;
		line-height: 22px;

	}
</style>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="container" id="content">

		<div class="row">

			<div class="col s12 content-area" id="primary">

				<main class="site-main" id="main" role="main">



				<!-- Page Layout here -->


<div id="container">
  <div class="row">
    <!-- Cards container -->
    <div class="col s12 m12 l12">

      <div id="card-container" class="row">
        <!-- Col: Card 1 -->
        <div class="col s12 m12 l12">
          <!-- Card 1 -->
          <div class="card recipe-card">
            <div class="card-content white-text">
              <span class="card-title grey-text text-darken-4">Mother's Unicorn Milk</span>
              <p class="card-subtitle grey-text text-darken-2">You will never taste anything as sweet and sexy as this tit milk.</p>
            </div>
            <div class="card-action">
              <a class="waves-effect red lighten-2 white-text btn-flat">Fruity</a>
							<a class="waves-effect yellow darken-2 white-text btn-flat">Dessert</a>
							<a class="waves-effect brown lighten-2 white-text btn-flat">Tobacco</a>
            </div>
          </div>
          <!-- End of card -->
        </div>
        <!-- End of col -->


      </div>
    </div>


				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->


	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>


<script type="text/javascript">

// 1. Recipe API, make request to recipe json
	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET', '/wp-json/wp/v2/recipes/');
	ourRequest.onload = function() {
		if (ourRequest.status >= 200 && ourRequest.status < 400) {
			var data = JSON.parse(ourRequest.responseText);
				// for each recipe..
				data.forEach( function (arrayItem) {
					rid = arrayItem.id;
					rname = arrayItem.title.rendered;
					rcontent = arrayItem.post_content;
					rflavorcatArray = arrayItem.acf.rFlavorCats;
					// Loop through flavor properties
					flavObjs = arrayItem.acf.recipeFlavors;
					flavObjArray = []
					$.each(flavObjs, function () {
        		flavObjtitle = this.recipeFlavor.post_title;
		    	});
				});
		} else {
			console.log("We connected to the server, but it returned an error.");
		}
	};
	ourRequest.onerror = function() {
		console.log("Connection error");
	};
	ourRequest.send();

</script>
