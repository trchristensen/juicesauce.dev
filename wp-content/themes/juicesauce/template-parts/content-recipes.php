<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Juicesauce
 */

?>

<!-- Collect the Variables -->

<?php
	$recipeName = get_the_title();
	$recipeNotes = get_the_content();
	$nicBaseMg = get_field('nicBaseMg');
	$nicvgRatio = get_field('nicBaseVg');
	$nicpgRatio = get_field('nicBasePg');
	$bottleMl = get_field('bottle_ml');
	$nicotineStrength = get_field('nic_mgml');
	$vgRatio = get_field('vg_ratio');
	$pgRatio = get_field('pg_ratio');
	$vgDrops = get_field('vg_drops');
	$vgMl = get_field('vg_ml');
	$vgG = get_field('vg_g');
	$pgDrops = get_field('pg_drops');
	$pgMl = get_field('pg_ml');
	$pgG = get_field('pg_g');
	$nicDrops = get_field('nic_drops');
	$nicMl = get_field('nic_ml');
	$nicG = get_field('nic_g');


	// Array of Flavors
	$recipeFlavors = get_field('recipeFlavors');

 ?>



<!-- <h1 id="num">

</h1> -->


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" style="text-indent:-999999px;position:absolute;">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'recipes' === get_post_type() ) : ?>
		<div class="entry-meta">

		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	  <div class="" id="wrapper">

			<div class="row">
				<div class="col l8 m12">
					<div class="components">

						<div v-show="!printable" id="base-components" class="boxee">

							<div class="row">
								<div class="input-field col s12">
									<input id="recipeName" type="text" class="validate" value="<?php echo $recipeName ?>">
									<label for="recipeName">Recipe Name</label>
								</div>
								<div class="input-field col s12">
									<textarea id="recipeNotes" class="materialize-textarea"><?php echo $recipeNotes ?></textarea>
									<label for="recipeNotes">Recipe Notes</label>
								</div>
							</div>

							<div class="row">
								<div class="section-heading">
									<h5 class="left-align">Nicotine Base</h5>
								</div>
								<div class="col s12 m4">
									<div class="input-field">
										<input v-model="nicBaseMg" id="nicBaseMg" type="number" min="0" class="validate" number>
										<label for="nicBaseMg">Nicotine MG/ML</label>
									</div>
								</div>
								<div class="col s12 m4">
									<div class="input-field">
										<input v-model="nicVG" id="nicBaseVg" @change="changeNicRatio('VG')" type="number" min="0" max="100" class="validate" number>
										<label for="nicBaseVg">VG ratio %</label>
									</div>
								</div>
								<div class="col s12 m4">
									<div class="input-field">
										<input v-model="nicPG" id="nicBasePg" @change="changeNicRatio('PG')" type="number" min="0" max="100" class="validate" number>
										<label for="nicBasePg">PG ratio %</label>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="section-heading">
									<h5 class="left-align">Target Measurements</h5>
								</div>
								<div class="col s6">
									<div class="input-field">
										<input id="bottle_ml" type="number" min="1" v-model="targetBottleSize" class="validate" number>
										<label for="bottle_ml">Bottle Size (ML)</label>
									</div>
								</div>

								<div class="col s6">
									<div class="input-field">
										<input v-model="targetNicMg" id="nic_mgml" type="number" min="0" max="{{nicBaseMg}}" class="" number>
										<label for="nic_mgml">Nicotine (mg/ml)</label>
									</div>
								</div>

								<div class="col s6">
									<div class="input-field">
										<input v-model="targetRatioVg" id="vg_ratio" type="number" min="0" @change="changeVgPg" max="100" class="vgpgratio validate" number>
										<label for="vg_ratio">VG Ratio (%)</label>
									</div>
								</div>

								<div class="col s6">
									<div class="input-field">
										<input v-model="targetRatioPg" id="pg_ratio" type="number" min="0" @change="changeVgPg" max="100" class="vgpgratio validate" number>
										<label for="pg_ratio">PG Ratio (%)</label>
									</div>
								</div>
							</div>

						</div>

						<div class="readable no-print row">
							<div class="col s12 m6">
								<p>
									Edit / Printable
									<div class="switch">
										<label>
												 Edit
												 <input v-model="printable" type="checkbox">
												 <span class="lever"></span>

											 </label>
									</div>
								</p>
							</div>
							<!-- <div v-show="printable" class="col s6 m3">
								<div class="input-field">
									<input v-model="printableFontSize" type="number" id="printableFontSize" min="5" max="30" disabled number>
									<label for="printableFontSize">Font Size</label>
								</div>
							</div> -->
							<!-- <div v-show="printable" class="col s6 m3">
								<a class="btn-floating btn-small waves-effect waves-light valign" @click="fontSizeChange('down')">
									<i class="material-icons">remove</i>
								</a>
								<a class="btn-floating btn-small waves-effect waves-light valign" @click="fontSizeChange('up')">
									<i class="material-icons">add</i>
								</a>
							</div> -->
						</div>


						<!-- Ingredients -->
						<ul class="collection" id="recipe-totals">

							<div v-if="printable" class="collection-item ingredient-header-line">
								<div class="row ingredient-header-line">
									<div class="col s12">
										<h2>{{recTitle}}</h2>
										<!-- <a class="faveIt" href=""><i class="fa-header-o fa" aria-hidden="true">Fave It</i></a> -->
										<?php if ( function_exists( 'wfp_button' ) ) wfp_button(); ?>
									</div>
								</div>
							</div>


							<li v-if="printable" class="collection-item ingredient-header-line">
								<div class="row ingredient-header-line">
									<div class="col s3">
										<p class="ing-heading">Bottle Size:<br /> {{targetBottleSize}}&nbsp;ML</p>
									</div>
									<div class="col s3">
										<p class="ing-heading">VG/PG Ratio:<br /> {{targetRatioVg}}/{{targetRatioPg}}</p>
									</div>
									<div class="col s6">
										<p class="ing-heading">Target Nicotine:<br /> {{targetNicMg}}&nbsp;MG - <br class="hide-on-med-and-up" /> ({{nicVG}}/{{nicPG}} {{nicBaseMg}}MG Base)</p>
									</div>
								</div>
							</li>

							<li class="collection-item ingredient-header-line">
								<div class="row ingredient-header-line">
									<div class="col s3">
										<p class="ing-heading">Ingredient</p>
									</div>
									<div class="col s3">
										<p class="ing-heading">drops</p>
									</div>
									<div class="col s3">
										<p class="ing-heading">milliliters</p>
									</div>
									<div class="col s3">
										<p class="ing-heading">grams</p>
									</div>

								</div>
							</li>
							<li class="collection-item" id="vg_line">
								<div class="row ingredient-line">
									<div class="col s3">
										<p class="name-val">VG</p>
									</div>
									<div class="col s3">
										<p class="name-val drops">{{leftoverVgMl | ml_to_drops | roundl 0}}</p>
									</div>
									<div class="col s3">
										<p class="name-val ml">{{leftoverVgMl | roundl}}</p>
									</div>
									<div class="col s3">
										<p class="name-val grams">{{leftoverVgMl | ml_to_grams 'VG' | roundl}}</p>
									</div>

								</div>
							</li>
							<li class="collection-item" id="pg_line">
								<div class="row ingredient-line">
									<div class="col s3">
										<p class="name-val">PG</p>
									</div>
									<div class="col s3">
										<p class="name-val drops">{{leftoverPgMl | ml_to_drops | roundl 0}}</p>
									</div>
									<div class="col s3">
										<p class="name-val ml">{{leftoverPgMl | roundl}}</p>
									</div>
									<div class="col s3">
										<p class="name-val grams">{{leftoverPgMl | ml_to_grams 'PG' | roundl}}</p>
									</div>

								</div>
							</li>
							<li class="collection-item" id="nic_line">
								<div class="row ingredient-line">
									<div class="col s3">
										<p class="name-val">Nicotine</p>
									</div>
									<div class="col s3">
										<p class="name-val drops">{{ nicPercentage | percent_to_ml | ml_to_drops | roundl 0 }}</p>
									</div>
									<div class="col s3">
										<p class="name-val ml">{{ nicPercentage | percent_to_ml | roundl }}</p>
									</div>
									<div class="col s3">
										<p class="name-val grams nic-grams">{{ nicPercentage | percent_to_ml | ml_to_grams "NIC" | roundl }}</p>
									</div>

								</div>
							</li>

							<li v-if="printable" v-for="flavor in flavors" class="collection-item">
								<div class="row ingredient-line">
									<div class="col s3">
										<p class="name-val">{{flavor.name}} - {{flavor.percentage | roundl 4 }}%</p>
									</div>
									<div class="col s3">
										<p class="name-val drops">{{ flavor.percentage | percent_to_ml | ml_to_drops | roundl 0 }}</p>
									</div>
									<div class="col s3">
										<p class="name-val ml">{{ flavor.percentage | percent_to_ml | roundl }}</p>
									</div>
									<div class="col s3">
										<p class="name-val grams">{{ flavor.percentage | percent_to_ml | ml_to_grams flavor.weight_src flavor.custom_weight | roundl }}</p>
									</div>

								</div>
							</li>


						</ul>

						<ul v-show="!printable" class="collapsible" id="flavors" data-collapsible="accordion">
							<li class="header">
								<div class="collapsible-header">
									<div class="row ingredient-header-line">
										<div class="col s3">
											<p class="ing-heading">Flavor Name</p>
										</div>
										<div class="col s3">
											<p class="ing-heading">drops</p>
										</div>
										<div class="col s3">
											<p class="ing-heading">milliliters</p>
										</div>
										<div class="col s3">
											<p class="ing-heading">grams</p>
										</div>

									</div>
								</div>
							</li>

							<!-- v-loop start -->

							<li v-for="flavor in flavors" class="flavor_line">
								<div class="collapsible-header">
									<div class="row ingredient-line">
										<div class="col s3 name">
											<p class="name-val">{{ flavor.name }} - {{flavor.percentage | roundl 4 }}%</p>
										</div>
										<div class="col s3 drops"><p>{{ flavor.percentage | percent_to_ml | ml_to_drops | roundl 0 }}</p></div>
										<div class="col s3 ml"><p>{{ flavor.percentage | percent_to_ml | roundl }}</p></div>
										<!-- <div class="col s3 grams">{{ ml_to_mg( percent_to_ml(flavor.percentage), flavor.weight_src, flavor.custom_weight ) }}</div> -->
										<div class="col s3 grams">
											<p><span class="grams">{{ flavor.percentage | percent_to_ml | ml_to_grams flavor.weight_src flavor.custom_weight | roundl }}</span>
												<i class="material-icons closed right hide-on-small-only">settings</i>
											</p>

										</div>



									</div>
								</div>
								<div class="collapsible-body">
									<div class="ingredient-body row">
										<div class="col s12">

											<div class="input-field col s12 m6">
												<i class="material-icons prefix">textsms</i>
												<label for="flavor-{{flavor.id}}" class="control-label">Flavor Name</label>
												<input class="form-control bs-autocomplete autocomplete-flavors" id="flavor-{{flavor.id}}" v-model="flavor.name" value="{{flavor.name}}" type="text" data-source="" data-hidden_field_id="flavor-id-{{flavor.id}}" data-item_id="id" data-item_label="label" autocomplete="off">
												<input class="form-control hidden-flavor-id" id="flavor-id-{{flavor.id}}" name="flavorid" value="" type="hidden" readonly>
											</div>


											<div class="input-field col s12 m6">
												<input id="flavor-percent-{{flavor.id}}" v-model="flavor.percentage" type="number" min="0" class="" number>
												<label for="flavor-percent-{{flavor.id}}" class="active">Percentage</label>
											</div>

											<!-- <div class="input-field col s12 m4">
												<select v-model="flavor.weight_src" class="browser-default">
													<option value="PG">PG Weight</option>
													<option value="VG">VG Weight</option>
													<option value="c">Custom</option>
												</select>

											</div> -->

											<!-- <div class="input-field col s12 m6" v-show="flavor.weight_src === 'c'">
												<input id="flavor-weight-{{flavor.id}}" v-model="flavor.custom_weight" type="number" min="0" class="" number>
												<label for="flavor-weight-{{flavor.id}}" class="active">Custom Weight (Grams per ML)</label>
											</div> -->

											<div class="col s12">
												<a class="waves-effect waves-light btn" @click="removeFlavor(flavor.id)">Delete</a>
												<a class="waves-effect waves-light btn flavor-close right">Close</a>
											</div>
										</div>
									</div>
								</div>
							</li>

						</ul>

						<div v-show="!printable" class="row">
							<div class="col m4 s6">
								<a class="waves-effect waves-light btn" id="addFlavor" @click="addFlavor">Add Flavor</a>
							</div>
						</div>

						<div v-show="printable" class="row">
							<div class="col s12">
								<p><strong>Recipe Notes:</strong></p>
								<div>{{{recNotes}}}</div>
							</div>
						</div>


						<!-- // Ingredients -->

					</div>

				</div>

				<div class="col l4 m12">
					<div id="flaveConcentrate" class="boxee">
						<h4>For Most Products, there will be a buy flavor concentrates button</h4>
						<p>Flavor concentrates, which are pre-mixed flavors (without the vg, pg, nic) will be sold to lazy fucks.</p>
						<a class="waves-effect waves-light btn" href="#">Buy Flavor Concentrates</a>
					</div>
					<div id="revisions" class="boxee">
						<h4>Revisions will go here</h4>
					</div>

				</div>
			</div>


			<?php


    // check if current user is the post author
    global $current_user;
    get_currentuserinfo();

    if (is_user_logged_in() && $current_user->ID == $post->post_author)  { ?>

			<div v-show="!printable" class="row center-align">
          <a class="waves-effect waves-light btn" id="updateRecipe">Update Recipe</a>
      </div>

	<?php
    }
?>

	  </div>



	<footer class="entry-footer">
		<?php juicesauce_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->


<?php


// check if current user is the post author
global $current_user;
get_currentuserinfo();

if (is_user_logged_in() && $current_user->ID == $post->post_author)  {

	//Here is the start of the magic code aka how I pick up girls "is yo daddy a baker cuz you got buns hun"
//get the motherfucking user ID
$user = wp_get_current_user();
$userID = $user->ID;
//so here you usually close it down to some roles but we going all out here so fuck this you already have it that only signed up people can see this anyways
// $allowed_roles = array('editor', 'administrator', 'author');
// if( array_intersect($allowed_roles, $user->roles ) ) {
//lets get this mother fucking scrip going ya?
?>


<script>
$(document).ready(function () {
//first we detect when the form gets submited
$( "#updateRecipe" ).click(function( e ) {
//we prevent from doing the default aka refreshing the page and send it via post to w/e page
e.preventDefault();

// GET THE RECIPE ID
var recipeID = <?php echo get_the_ID(); ?>;
//then we get the values from the form fields
var recipeName = $("#recipeName").val();
var recipeNotes = $("#recipeNotes").val();
//since I am a bit fucking drunk I am going to do 2 you can do the rest, I believe in you bruh

// Custom Fields
var nicBaseMg = $('#nicBaseMg').val();
var nicBaseVg = $('#nicBaseVg').val();
var nicBasePg = $('#nicBasePg').val();
var bottle_ml = $('#bottle_ml').val();
var nic_mgml = $('#nic_mgml').val();
var vg_ratio = $('#vg_ratio').val();
var pg_ratio = $('#pg_ratio').val();
var vg_drops = $('#vg_line p.name-val.drops').text();
var vg_ml = $('#vg_line p.name-val.ml').text();
var vg_g = $('#vg_line p.name-val.grams').text();
var pg_drops = $('#pg_line p.name-val.drops').text();
var pg_ml = $('#pg_line p.name-val.ml').text();
var pg_g = $('#pg_line p.name-val.grams').text();
var nic_drops = $('#nic_line p.name-val.drops').text();
var nic_ml = $('#nic_line p.name-val.ml').text();
var nic_g = $('#nic_line p.name-val.grams').text();

// Repeater
var recipeFlavors = [];

// New loop to get all info for each flavor instead of just the flavor
	$( ".flavor_line" ).each(function( index ) {
		fDrops = $(this).find(".drops p").text();
		fMl = $(this).find(".ml p").text();
		fG = $(this).find(".grams .grams").text();
		rId = $(this).find("input.hidden-flavor-id").val();
		flavor_perc = $(this).find(".flavor_percentage").val();

		// Push recipeFlavor object onto recipeFlavors
		recipeFlavors.push({recipeFlavor:{id: rId},flavor_drops: fDrops,flavor_ml: fMl,flavor_g: fG, flavor_perc: flavor_perc});
	});


//Then we get the WordPress Ajax URL
var ajaxurl = 'http://'+window.location.host+'/wp-admin/admin-ajax.php';
// this mother fucker will help up send all the data and use a function in function.php to make a post
	//Ajax thing
	$.ajax({
									//we just doing normal ajax from wordpress and an action meaning a function in functions.php
									url: ajaxurl + "?action=updateJuice",
									//here is the info we should be sending aka content  and crazy advanced custom fields
									//so in the example below is pretty easy recipeName: is the name of the post and the next variable is the date so is  namePost : data
									/**
									 *  Example is recipeName : recipeName - the 1st recipeName is the variable that you are passing to functions.php and the next one is the variable that you
									 * made above getting the data from JQuery so each time you want to add an extra variable/form field you get the value of it via jquery and then send it to
									 * functions.php via ajax in the data array
									 *  also you could add all this into one array and just foreach on functions.php if you send all this as a big json object
									 * */
									// the last one is userID we need that so we can create the post and assign this as the author
									data: {recipeID: recipeID, recipeName: recipeName, recipeNotes: recipeNotes, userID: <?php echo $userID; ?>, fields: {nicBaseMg: nicBaseMg, nicBaseVg: nicBaseVg, nicBasePg: nicBasePg, bottle_ml: bottle_ml, nic_mgml: nic_mgml, vg_ratio: vg_ratio, pg_ratio: pg_ratio, vg_drops: vg_drops, vg_ml: vg_ml, vg_g: vg_g, pg_drops: pg_drops, pg_ml: pg_ml, pg_g: pg_g, nic_drops: nic_drops, nic_ml: nic_ml, nic_g: nic_g, recipeFlavors: recipeFlavors},},
									//type of request, usually post
									type: 'POST',
									success: function(data) {
									window.location.href = '/' + data;
									console.log(data);
									},
									error: function(xhr, ajaxOptions, thrownerror) { }
					}); //ajax function
	}); //submit function


}); //doc ready
//donezo cya in functions.php
</script>

<?php } ?>
