<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Juicesauce
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer light-blue darken-2" role="contentinfo">
		<div class="site-info">
			<div class="s12 center white-text">
				<p><?php echo comicpress_copyright(); ?> JuiceSauce.com</p>
			</div>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->



<?php wp_footer(); ?>

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script> -->

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> -->

<script type="text/javascript">

$( document ).ready(function(){

	$('.tooltipped').tooltip({delay: 50});

	// Initialize collapse button
	$(".button-collapse").sideNav();
	// Initialize collapsible (uncomment the line below if you use the dropdown variation)
	$('.collapsible').collapsible();

		$(".dropdown-button").dropdown({
			inDuration: 300,
			outDuration: 225,
			constrainWidth: false, // Does not change width of dropdown to that of the activator
			hover: true, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'left', // Displays dropdown with edge aligned to the left of button
			stopPropagation: false // Stops event propagation
		});
		// $('.dropdown-button').mouseOut(function(){});

		$(".recipeDropdown-button").dropdown({
		 inDuration: 300,
		 outDuration: 225,
		 constrainWidth: false, // Does not change width of dropdown to that of the activator
		 hover: true, // Activate on hover
		 gutter: 0, // Spacing from edge
		 belowOrigin: true, // Displays dropdown below the button
		 alignment: 'right', // Displays dropdown with edge aligned to the left of button
		 stopPropagation: false // Stops event propagation
		});







})


</script>


</body>
</html>
