<?php
/**
 * Template Name: Flavors
 *
 *
 * @package understrap
 */


get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<style media="screen">
	.recipe-card .card-action .btn-flat {
		font-size: 12px;
		padding: 0 1rem;
		margin-right: 6px !important;
		height: initial;
		line-height: 22px;

	}
</style>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="" id="content">



		<div class="row">

			<div class="col s12 content-area" id="primary">

				<main class="site-main" style="margin-top: 2rem;" id="main" role="main">

				<!-- Page Layout here -->


<div id="container">
  <div class="row">



    <!-- Cards container -->
		<div class="col s12 m12 l3 card facet-filter transparent">

			<?php echo facetwp_display( 'facet', 'recipe_search' ); ?>

			<!-- Sorter -->
			<h5>Sort by</h5>
			<?php echo facetwp_display( 'sort' ) ?>

			<h5>Flavor Categories</h5>
			 <?php echo facetwp_display( 'facet', 'example' ); ?>

			 <h5>Flavors</h5>
			 <?php echo facetwp_display( 'facet', 'flavors' ); ?>

			 <h5>Mixologists</h5>
			 <?php echo facetwp_display( 'facet', 'mixologist' ); ?>


			 <a class="btn green lighten-2" onclick="FWP.reset()">Reset</a>

		</div>


    <div class="col s12 m12 l9">
			<div id="card-container white" class="row z-depth">
      	<?php echo facetwp_display( 'template', 'flavors' ); ?>

				<?php echo facetwp_display( 'pager' ); ?>
			</div>
    </div>


				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->


	</div><!-- Container end -->

</div><!-- Wrapper end -->

</div>
</div>

<?php get_footer(); ?>
