jQuery(function( $ ) {
  // $( document ).ready(function() {


  ////////////////////////////////////////////////////////////////
  // EDIT/SINGLE RECIPE CALCULATOR SCRIPT (EXISTING RECIPE)
  ////////////////////////////////////////////////////////////////
  if ($("body").hasClass("single-recipes")) {


    var calculate = {
      ml_to_mg: function(ml, weight) {
        return ml * weight;
      },
      percent_to_ml: function(bottle_size, percentage) {
        return bottle_size * ( parseFloat(percentage) / 100 );
      }
    };


    var vm, $list;

    // Vue.config.debug = true;

    Vue.filter('percent_to_ml', function (percentage) {
      return this.targetBottleSize * ( parseFloat(percentage) / 100 );
    });

    Vue.filter('ml_to_grams', function (ml, type, custom) {
      if(type === "PG")               { return ml * parseFloat(this.pgWeight); }
      else if(type === "VG")          { return ml * parseFloat(this.vgWeight); }
      else if(custom || custom === 0) { return ml * parseFloat(custom); }
      else if(type == "NIC") {
        return ml * parseFloat(this.nicWeight);
      }
    });

    Vue.filter('leftover_pg_ml', function (flavor_percentage) {
      return this.targetBottleSize * ( (parseFloat(this.targetRatioPg) - parseFloat(flavor_percentage)) / 100 );
    });

    Vue.filter('leftover_vg_ml', function (flavor_percentage) {
      return this.targetBottleSize * ( (parseFloat(this.targetRatioVg) - parseFloat(flavor_percentage)) / 100 );
    });

    Vue.filter('ml_to_drops', function (ml) {
      return ml * this.dropsPerMl;
    });

    Vue.filter('roundl', function (val, points) {
      return _.round(val, (points || points === 0) ? points : 3);
    });


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Getting the ID of the RECIPE (this is for the single recipe page.
// We need to pull the values from the database but we need to figure out which recipe it is before we make an API request.)


  var id, matches = document.body.className.match(/(^|\s)postid-(\d+)(\s|$)/);
  if (matches) {
    // found the id
    id = matches[2];

    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', '/wp-json/wp/v2/recipes/'+id);
    ourRequest.onload = function() {
      if (ourRequest.status >= 200 && ourRequest.status < 400) {
        var data = JSON.parse(ourRequest.responseText);

        // console.log(data);

            var recTitle = data.title.rendered;
            var recNotes = data.content.rendered;
            var recNicBaseMg = data.acf.nicBaseMg;
            var recNicBasePg = data.acf.nicBasePg;
            var recNicBaseVg = data.acf.nicBasePg;
            var recBottle_ml = data.acf.bottle_ml;
            var recNic_mgml = data.acf.nic_mgml;
            var recVg_ratio = data.acf.vg_ratio;
            var recPg_ratio = data.acf.pg_ratio;




            var recipeFlavors = data.acf.recipeFlavors;

            //Recipe Object Looopingg
           var flaveArray = [];

            // for (var i = 0, l = data.acf.recipeFlavors.length; i < l; i++) {
             for(var x in data.acf.recipeFlavors){

               var flaveObj = {};

                var flavorDrops = data.acf.recipeFlavors[x].flavor_drops;
                var flavorPerc = data.acf.recipeFlavors[x].flavor_perc;
                var flavor_ml = data.acf.recipeFlavors[x].flavor_ml;
                var flavor_g = data.acf.recipeFlavors[x].flavor_g;

                 var recipeFlavorName = data.acf.recipeFlavors[x].recipeFlavor['post_title'];
                 var recipeFlavorID = data.acf.recipeFlavors[x].recipeFlavor['ID'];

                   //  pushing flavor values into new object.
                     flaveObj.name = recipeFlavorName;
                     flaveObj.percentage = flavorPerc;
                     flaveObj.weight_src = "PG";
                     flaveObj.custom_weight = 1;
                     flaveObj.id = _.uniqueId();
                     flaveObj.postID = recipeFlavorID;

                   // pushing object into array
                   flaveArray.push(flaveObj);

            }

            //  console.log(flaveArray);

  // Grab Parent Recipe Data from hidden fields
  var pnicBaseMg = $('#pnicBaseMg').val();
  if (pnicBaseMg == "") {pnicBaseMg = 0};

  var pnicBaseVg = $('#pnicBaseVg').val();
  if (pnicBaseVg == "") {pnicBaseVg = 0};

  var pnicBasePg = $('#pnicBasePg').val();
  if (pnicBasePg == "") {pnicBasePg = 0};

  var pbottleml = $('#pbottleml').val();
  if (pbottleml == "") {pbottleml = 0};

  var pnicmgml = $('#pnicmgml').val();
  if (pnicmgml == "") {pnicmgml = 0};

  var pvgratio = $('#pvgratio').val();
  if (pvgratio == "") {pvgratio = 0};

  var ppgratio = $('#ppgratio').val();
  if (ppgratio == "") {ppgratio = 0};

  var pvgdrops = $('#pvgdrops').val();
  if (pvgdrops == "") {pvgdrops = 0};

  var ppgdrops = $('#ppgdrops').val();
  if (ppgdrops == "") {ppgdrops = 0};

  var ppgml = $('#ppgml').val();
  if (ppgml == "") {ppgml = 0};

  var ppgg = $('#ppgg').val();
  if (ppgg == "") {ppgg = 0};

  var pnicdrops = $('#pnicdrops').val();
  if (pnicdrops == "") {pnicdrops = 0};

  var pnicml = $('#pnicml').val();
  if (pnicml == "") {pnicml = 0};

  var pnicg = $('#pnicg').val();
  if (pnicg == "") {pnicg = 0};

  var ppgml = $('#ppgml').val();
  if (ppgml == "") {ppgml = 0};

  var pvgml = $('#pvgml').val();
  if (pvgml == "") {pvgml = 0};


  var settingsDefaults = {
    printable: true,
    // printable: false,

    recTitle: recTitle,
    recNotes: recNotes,

    vgWeight: 1.249,
    pgWeight: 1.0361,
    dropsPerMl: 30,

    targetBottleSize: recBottle_ml,
    targetNicMg: recNic_mgml,

    nicBaseMg: recNicBaseMg,
    nicVG: recNicBaseVg,
    nicPG: recNicBasePg,
    nicPureMg: 1.01,
    nicWeight: 0,
    nicCustomWeight: 0,

    weightSource: 'default',

    targetRatioVg: recVg_ratio,
    targetRatioPg: recPg_ratio,
    flavors: flaveArray,
    // flavors: [
    //   { name: "Flavor", percentage: 0, weight_src: "PG", custom_weight: 1, id: _.uniqueId() }
    // ],

    // parent data
    parentRecipe: {
      pnicBaseMg: pnicBaseMg,
      pnicBaseVg: pnicBaseVg,
      pnicBasePg: pnicBasePg,
      pbottleml: pbottleml,
      pnicmgml: pnicmgml,
      pvgratio: pvgratio,
      ppgratio: ppgratio,
      pvgdrops: pvgdrops,
      ppgdrops: ppgdrops,
      ppgml: ppgml,
      ppgg: ppgg,
      pnicdrops: pnicdrops,
      pnicml: pnicml,
      pnicg: pnicg,
      pvgml : pvgml,
    },

    weightOptions: {
      default: {
        title: 'Default',
        details: "These are the measurements that I've seen the most on the internet.",
        vgWeight: 1.249,
        pgWeight: 1.0361
      },
      amazon_essentials: {
        title: 'Amazon Essentials',
        details: 'These measurement are directly from the manufacturer of the top selling VG and PG 1QT bottles on Amazon. (Personally, I use these measurements)',
        vgWeight: 1.2881,
        pgWeight: 1.07887
      },
      botboy141: {
        title: 'botboy141',
        details: "This reddit user knows their stuff and has paved the way for a lot of us DIY'ers. https://www.reddit.com/r/DIY_eJuice/comments/2iq3km/botboy141_guide_to_mixing_by_weight/",
        vgWeight: 1.26,
        pgWeight: 1.038
      },
      v_ecigs: {
        title: 'v-ecigs.com',
        details: 'This is another well regarded source and is taken from their list here. http://www.v-ecigs.com/tfa-flavor-percentage-recommendations/',
        vgWeight: 1.261,
        pgWeight: 1.036
      }
    },

    printableFontSize: 14,

  };



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    //Fetch the settings object. If it's empty, assign an empty object
    var settingsUser = JSON.parse(localStorage.getItem('settings') || '{}');
    // @todo _.isObject check?

    //Merge the default settings into the user settings (for missing values)
    settingsUser = _.defaultsDeep(settingsUser, settingsDefaults);

    /**
    * @todo settingsUser change listener/updater
    * @todo should probably seperate each value to it's own localStorage property for more control
    */
    // if(!_.isEqual(settingsUser, settingsDefaults)) {
    //   localStorage.setItem('settings', JSON.stringify(settingsUser));
    // }


    vm = new Vue({
        el: '#wrapper',

        data: settingsUser,

        watch: {
          weightSource: function(val, oldVal) {
            if(val == "custom") {
              return;
            }
            this.vgWeight = this.weightOptions[val].vgWeight;
            this.pgWeight = this.weightOptions[val].pgWeight;
          },
          printableFontSize: function (val, oldVal) {
            $(".print-mode #recipe-totals").css('font-size', val + "px");
          },
          printable: function(val, oldVal) {
            if(val === true) {

              $(".print-mode #recipe-totals").css(
                'font-size', this.printableFontSize + "px"
              );

              $("body").addClass('print-mode');


            } else {
              $("body").removeClass('print-mode');
            }
          }
        },



        computed: {
          numClass() {
             if ( this.parentRecipe.pbottleml == 0) {
               return 'hide';
             }
             return 'name';
           },

          leftoverVgMl: {
            cache: false,
            get: function() {
              return this.targetBottleSize * (
                (
                  parseFloat(this.targetRatioVg) - parseFloat(this.flavorsSum.vg) - parseFloat(this.nicPercentageLiquids.vg)
                ) / 100 );
            }
          },
          leftoverPgMl: {
            cache: false,
            get: function() {
              return this.targetBottleSize * (
                (
                  parseFloat(this.targetRatioPg) -  parseFloat(this.flavorsSum.pg) - parseFloat(this.nicPercentageLiquids.pg)
                ) / 100 );
            }
          },

          nicPercentage: {
            cache: false,
            get: function() {
              return (this.targetNicMg / this.nicBaseMg) * 100;
            }
          },

          nicPercentageLiquids: {
            cache: false,
            get: function() {
              var total = this.nicPercentage;
              return {
                vg: total * (this.nicVG / 100),
                pg: total * (this.nicPG / 100)
              };
            }
          },

          nicPercentages: {
            cache: false,
            get: function() {
              var nic_percent = (this.nicBaseMg / 10);
              return {
                pg: (this.nicPG) - ((nic_percent / 10) * (this.nicPG / 10)),
                vg: (this.nicVG) - ((nic_percent / 10) * (this.nicVG / 10)),
                nic: nic_percent
              };
            }
          },
          nicWeight: {
            cache: false,
            /**
            * Calculates the weight of the nicotine base, based on the nicotine weight
            * devided into it's respective VG or PG percentages accordingly (does not just split it 50/50)
            */
            get: function() {
              return  (
                        (this.pgWeight  * (this.nicPercentages.pg / 10)) +
                        (this.vgWeight  * (this.nicPercentages.vg / 10)) +
                        (this.nicPureMg * (this.nicPercentages.nic / 10))
                      ) / 10;
            }
          },

          flavorsSum: {
            cache: false,
            get: function() {
              return {
                all: _.sumBy(this.flavors, function(o){
                  return parseFloat(o.percentage);
                }),
                // Sum VG flavors
                pg: _.sumBy(this.flavors, function(o){
                  return (o.weight_src == "VG") ? 0: parseFloat(o.percentage);
                }),
                //Sum PG flavors
                vg: _.sumBy(this.flavors, function(o){
                  return (o.weight_src == "VG") ? parseFloat(o.percentage): 0;
                })
              };
            }
          },
          flavorSumPercent: {
            cache: false,
            get: function() {
              return flavorSum.all;
            }
          }
        },
        methods: {

          fontSizeChange: function(direction) {
            if(direction == "up") {
              if(this.printableFontSize > 26) return;
              this.printableFontSize += 1;
            } else {
              if(this.printableFontSize < 8) return;
              this.printableFontSize -= 1;
            }
          },

          weightSetCustom: function() {
            this.weightSource = 'custom';
          },

          //Function for the change event on the vg/pg target ratio inputs
          changeVgPg: function(e) {
            funcs = {
              'pg_ratio': function() { this.targetRatioVg = 100 - this.targetRatioPg; }.bind(this),
              'vg_ratio': function() { this.targetRatioPg = 100 - this.targetRatioVg; }.bind(this)
            };
            //Us the element ID as the function name
            funcs[e.target.id]();
          },

          // Function for the change event on the vg/pg base nicotine ratio inputs
          changeNicRatio: function(type) {
            funcs = {
              'PG': function() { this.nicVG = 100 - this.nicPG; }.bind(this),
              'VG': function() { this.nicPG = 100 - this.nicVG; }.bind(this)
            };
            //Us the element ID as the function name
            funcs[type]();
          },

          addFlavor: function() {
            this.flavors.push({
              name: "",
              percentage: 0,
              weight_src: "PG",
              custom_weight: 1,
              id: _.uniqueId(),
              flavorsList: [
                {text: "Strawberry", value: "30"},
                {text: "Mochi", value: "28"}
              ]
            });
          },

          removeFlavor: function(id) {
            this.flavors = _.filter(this.flavors, function(flavor) {
              return (flavor.id == id) ? false : true;
            });
          },

          percent_to_ml: function(percentage) {
            return this.targetBottleSize * ( parseFloat(percentage) / 100 );
          },

          ml_to_mg: function(ml, weightSrc, custom) {
            if(weightSrc == "PG") { weight = this.pgWeight; } else
            if(weightSrc == "VG") { weight = this.vgWeight; } else
            if(custom)            { weight = custom; }

            return parseFloat(ml) * parseFloat(weight);
          }

        }
    });

    function printable() {
      // $(".components > *:not('#recipe-totals')").hide();
      $("body").addClass('print-mode');
      vm.$set('printable', true);
    }

    function unPrintable() {
      // $(".components > *:not('#recipe-totals')").show();
      $("body").removeClass('print-mode');
      vm.$set('printable', false);
    }


      //Init elements
      // $('.modal-trigger').leanModal();

      // Flavor close button
      $("body").on('click', '.flavor-close', function(){
        $( $(this).parents('.collapsible-body').siblings()[0] ).trigger('click');
      });
//END CALCULATOR SCRIPT

    } else {
      console.log("We connected to the server, but it returned an error.");
    }
    };
    ourRequest.onerror = function() {
    console.log("Connection error");
    };
    ourRequest.send();
    }

  } else {

////////////////////////////////////////////////////////////////
// REGULAR CALCULATOR SCRIPT (ADDING NEW RECIPE)
////////////////////////////////////////////////////////////////

    var calculate = {
      ml_to_mg: function(ml, weight) {
        return ml * weight;
      },
      percent_to_ml: function(bottle_size, percentage) {
        return bottle_size * ( parseFloat(percentage) / 100 );
      }
    };


    var vm, $list;

    // Vue.config.debug = true;

    Vue.filter('percent_to_ml', function (percentage) {
      return this.targetBottleSize * ( parseFloat(percentage) / 100 );
    });

    Vue.filter('ml_to_grams', function (ml, type, custom) {
      if(type === "PG")               { return ml * parseFloat(this.pgWeight); }
      else if(type === "VG")          { return ml * parseFloat(this.vgWeight); }
      else if(custom || custom === 0) { return ml * parseFloat(custom); }
      else if(type == "NIC") {
        return ml * parseFloat(this.nicWeight);
      }
    });

    Vue.filter('leftover_pg_ml', function (flavor_percentage) {
      return this.targetBottleSize * ( (parseFloat(this.targetRatioPg) - parseFloat(flavor_percentage)) / 100 );
    });

    Vue.filter('leftover_vg_ml', function (flavor_percentage) {
      return this.targetBottleSize * ( (parseFloat(this.targetRatioVg) - parseFloat(flavor_percentage)) / 100 );
    });

    Vue.filter('ml_to_drops', function (ml) {
      return ml * this.dropsPerMl;
    });

    Vue.filter('roundl', function (val, points) {
      return _.round(val, (points || points === 0) ? points : 3);
    });


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  // Initialize values
      var settingsDefaults = {
        printable: false,

        vgWeight: 1.249,
        pgWeight: 1.0361,
        dropsPerMl: 30,

        targetBottleSize: 30,
        targetNicMg: 3,

        nicBaseMg: 100,
        nicVG: 70,
        nicPG: 30,
        nicPureMg: 1.01,
        nicWeight: 0,
        nicCustomWeight: 0,

        weightSource: 'default',

        targetRatioVg: 70,
        targetRatioPg: 30,
        flavors: [
          { name: "Flavor", percentage: 0, weight_src: "PG", custom_weight: 1, id: _.uniqueId() }
        ],

        weightOptions: {
          default: {
            title: 'Default',
            details: "These are the measurements that I've seen the most on the internet.",
            vgWeight: 1.249,
            pgWeight: 1.0361
          },
          amazon_essentials: {
            title: 'Amazon Essentials',
            details: 'These measurement are directly from the manufacturer of the top selling VG and PG 1QT bottles on Amazon. (Personally, I use these measurements)',
            vgWeight: 1.2881,
            pgWeight: 1.07887
          },
          botboy141: {
            title: 'botboy141',
            details: "This reddit user knows their stuff and has paved the way for a lot of us DIY'ers. https://www.reddit.com/r/DIY_eJuice/comments/2iq3km/botboy141_guide_to_mixing_by_weight/",
            vgWeight: 1.26,
            pgWeight: 1.038
          },
          v_ecigs: {
            title: 'v-ecigs.com',
            details: 'This is another well regarded source and is taken from their list here. http://www.v-ecigs.com/tfa-flavor-percentage-recommendations/',
            vgWeight: 1.261,
            pgWeight: 1.036
          }
        },

        printableFontSize: 14,

      };


    //Fetch the settings object. If it's empty, assign an empty object
    var settingsUser = JSON.parse(localStorage.getItem('settings') || '{}');
    // @todo _.isObject check?

    //Merge the default settings into the user settings (for missing values)
    settingsUser = _.defaultsDeep(settingsUser, settingsDefaults);

    /**
    * @todo settingsUser change listener/updater
    * @todo should probably seperate each value to it's own localStorage property for more control
    */
    // if(!_.isEqual(settingsUser, settingsDefaults)) {
    //   localStorage.setItem('settings', JSON.stringify(settingsUser));
    // }


    vm = new Vue({
        el: '#wrapper',

        data: settingsUser,

        watch: {
          weightSource: function(val, oldVal) {
            if(val == "custom") {
              return;
            }
            this.vgWeight = this.weightOptions[val].vgWeight;
            this.pgWeight = this.weightOptions[val].pgWeight;
          },
          printableFontSize: function (val, oldVal) {
            $(".print-mode #recipe-totals").css('font-size', val + "px");
          },
          printable: function(val, oldVal) {
            if(val === true) {

              $(".print-mode #recipe-totals").css(
                'font-size', this.printableFontSize + "px"
              );

              $("body").addClass('print-mode');

            } else {
              $("body").removeClass('print-mode');
            }
          }
        },

        computed: {
          leftoverVgMl: {
            cahce: false,
            get: function() {
              return this.targetBottleSize * (
                (
                  parseFloat(this.targetRatioVg) - parseFloat(this.flavorsSum.vg) - parseFloat(this.nicPercentageLiquids.vg)
                ) / 100 );
            }
          },
          leftoverPgMl: {
            cache: false,
            get: function() {
              return this.targetBottleSize * (
                (
                  parseFloat(this.targetRatioPg) -  parseFloat(this.flavorsSum.pg) - parseFloat(this.nicPercentageLiquids.pg)
                ) / 100 );
            }
          },

          nicPercentage: {
            cache: false,
            get: function() {
              return (this.targetNicMg / this.nicBaseMg) * 100;
            }
          },

          nicPercentageLiquids: {
            cache: false,
            get: function() {
              var total = this.nicPercentage;
              return {
                vg: total * (this.nicVG / 100),
                pg: total * (this.nicPG / 100)
              };
            }
          },

          nicPercentages: {
            cache: false,
            get: function() {
              var nic_percent = (this.nicBaseMg / 10);
              return {
                pg: (this.nicPG) - ((nic_percent / 10) * (this.nicPG / 10)),
                vg: (this.nicVG) - ((nic_percent / 10) * (this.nicVG / 10)),
                nic: nic_percent
              };
            }
          },
          nicWeight: {
            cache: false,
            /**
            * Calculates the weight of the nicotine base, based on the nicotine weight
            * devided into it's respective VG or PG percentages accordingly (does not just split it 50/50)
            */
            get: function() {
              return  (
                        (this.pgWeight  * (this.nicPercentages.pg / 10)) +
                        (this.vgWeight  * (this.nicPercentages.vg / 10)) +
                        (this.nicPureMg * (this.nicPercentages.nic / 10))
                      ) / 10;
            }
          },

          flavorsSum: {
            cache: false,
            get: function() {
              return {
                all: _.sumBy(this.flavors, function(o){
                  return parseFloat(o.percentage);
                }),
                // Sum VG flavors
                pg: _.sumBy(this.flavors, function(o){
                  return (o.weight_src == "VG") ? 0: parseFloat(o.percentage);
                }),
                //Sum PG flavors
                vg: _.sumBy(this.flavors, function(o){
                  return (o.weight_src == "VG") ? parseFloat(o.percentage): 0;
                })
              };
            }
          },
          flavorSumPercent: {
            cache: false,
            get: function() {
              return flavorSum.all;
            }
          }
        },
        methods: {

          fontSizeChange: function(direction) {
            if(direction == "up") {
              if(this.printableFontSize > 26) return;
              this.printableFontSize += 1;
            } else {
              if(this.printableFontSize < 8) return;
              this.printableFontSize -= 1;
            }
          },

          weightSetCustom: function() {
            this.weightSource = 'custom';
          },

          //Function for the change event on the vg/pg target ratio inputs
          changeVgPg: function(e) {
            funcs = {
              'pg_ratio': function() { this.targetRatioVg = 100 - this.targetRatioPg; }.bind(this),
              'vg_ratio': function() { this.targetRatioPg = 100 - this.targetRatioVg; }.bind(this)
            };
            //Us the element ID as the function name
            funcs[e.target.id]();
          },

          // Function for the change event on the vg/pg base nicotine ratio inputs
          changeNicRatio: function(type) {
            funcs = {
              'PG': function() { this.nicVG = 100 - this.nicPG; }.bind(this),
              'VG': function() { this.nicPG = 100 - this.nicVG; }.bind(this)
            };
            //Us the element ID as the function name
            funcs[type]();
          },

          addFlavor: function() {
            this.flavors.push({
              name: "",
              percentage: 0,
              weight_src: "PG",
              custom_weight: 1,
              id: _.uniqueId()
            });
          },

          removeFlavor: function(id) {
            this.flavors = _.filter(this.flavors, function(flavor) {
              return (flavor.id == id) ? false : true;
            });
          },

          percent_to_ml: function(percentage) {
            return this.targetBottleSize * ( parseFloat(percentage) / 100 );
          },

          ml_to_mg: function(ml, weightSrc, custom) {
            if(weightSrc == "PG") { weight = this.pgWeight; } else
            if(weightSrc == "VG") { weight = this.vgWeight; } else
            if(custom)            { weight = custom; }

            return parseFloat(ml) * parseFloat(weight);
          }

        }
    });

    function printable() {
      // $(".components > *:not('#recipe-totals')").hide();
      $("body").addClass('print-mode');
      vm.$set('printable', true);
    }

    function unPrintable() {
      // $(".components > *:not('#recipe-totals')").show();
      $("body").removeClass('print-mode');
      vm.$set('printable', false);
    }


      //Init elements
      // $('.modal-trigger').leanModal();

      // Flavor close button
      $("body").on('click', '.flavor-close', function(){
        $( $(this).parents('.collapsible-body').siblings()[0] ).trigger('click');
      });
//END CALCULATOR SCRIPT



  };




// jQuery End
  // });
});
