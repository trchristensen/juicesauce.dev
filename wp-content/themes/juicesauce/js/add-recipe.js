jQuery(function( $ ) {
  $( document ).ready(function() {


//Update field labels to active/not-active on page load.
Materialize.updateTextFields();

// 1. Flavor API, make request to flavor json
  var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', '/wp-json/wp/v2/flavors/');
  ourRequest.onload = function() {
    if (ourRequest.status >= 200 && ourRequest.status < 400) {
      var data = JSON.parse(ourRequest.responseText);
      // 2. Create array of objects of ALL flavors for autocomplete
      var flaves = [];
        // for each flavor..
        data.forEach( function (arrayItem) {
          var fid = arrayItem.id;
          var fname = arrayItem.title.rendered;
          var flavorbase = arrayItem.acf.flavor_base;
          flaves.push({id: fid, label: fname, fbase: flavorbase });
        });
        // turns the array of objects into json format
        jsonflaves = JSON.stringify(flaves);

      // 3. Flavor autocomplete function
      $.widget("ui.autocomplete", $.ui.autocomplete, {

        _renderMenu: function(ul, items) {
          var that = this;
          ul.attr("class", "nav nav-pills nav-stacked  bs-autocomplete-menu");
          $.each(items, function(index, item) {
            that._renderItemData(ul, item);
          });
        },

        _resizeMenu: function() {
          var ul = this.menu.element;
          ul.outerWidth(Math.min(
            // Firefox wraps long text (possibly a rounding bug)
            // so we add 1px to avoid the wrapping (#7513)
            ul.width("").outerWidth() + 1,
            this.element.outerWidth()
          ));
        }
      });

      (function autocompleteFlavors() {
        "use strict";

        $('.bs-autocomplete').each(
          function bsAutocomplete() {
          var _this = $(this),
            _data = _this.data(),
            _hidden_field = $('#' + _data.hidden_field_id);
          _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
            .parent('.form-group').addClass('has-feedback');
          var feedback_icon = _this.next('.bs-autocomplete-feedback');
          feedback_icon.hide();
          _this.autocomplete({
              minLength: 1,
              autoFocus: true,
              source: function(request, response) {
                var _regexp = new RegExp(request.term, 'i');
                var data = flaves.filter(function(item) {
                  return item.label.match(_regexp);
                });
                response(data);
              },
              search: function() {
                feedback_icon.show();
                _hidden_field.val('');
              },
              response: function() {
                feedback_icon.hide();
              },
              focus: function(event, ui) {
                _this.val(ui.item[_data.item_label]);
                event.preventDefault();
              },
              select: function(event, ui) {
                _this.val(ui.item[_data.item_label]);
                _hidden_field.val(ui.item[_data.item_id]);
                event.preventDefault();
              },
              change: function (event, ui) {
                  if (!ui.item) {
                      this.value = '';
                      $('.flavor_line.active .name-val').css('color', 'red');
                      alert('Please make sure you select an existing flavor! Only exisiting flavors may be used.');
                    }
                  else{
                   $('.flavor_line.active .name-val').css('color', '#404040;');
                  }
              }
            })
            .data('ui-autocomplete')._renderItem = function(ul, item) {
              return $('<li></li>')
                .data("item.autocomplete", item)
                .append('<a>' + item[_data.item_label] + '</a>')
                .appendTo(ul);
            };
          // end autocomplete
        });

        // Repeating autocomplete function for dynamically added flavors (after the first one).
        $("#addFlavor").click(function(){
          //Update field labels to active/not-active on page load.
          Materialize.updateTextFields();

          $('.bs-autocomplete').each(
            function bsAutocomplete() {
            var _this = $(this),
              _data = _this.data(),
              _hidden_field = $('#' + _data.hidden_field_id);
            _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
              .parent('.form-group').addClass('has-feedback');
            var feedback_icon = _this.next('.bs-autocomplete-feedback');
            feedback_icon.hide();
            _this.autocomplete({
                minLength: 1,
                autoFocus: true,
                source: function(request, response) {
                  var _regexp = new RegExp(request.term, 'i');
                  var data = flaves.filter(function(item) {
                    return item.label.match(_regexp);
                  });
                  response(data);
                },
                search: function() {
                  feedback_icon.show();
                  _hidden_field.val('');
                },
                response: function() {
                  feedback_icon.hide();
                },
                focus: function(event, ui) {
                  _this.val(ui.item[_data.item_label]);
                  event.preventDefault();
                },
                select: function(event, ui) {
                  _this.val(ui.item[_data.item_label]);
                  _hidden_field.val(ui.item[_data.item_id]);
                  event.preventDefault();
                },
                change: function (event, ui) {
                  if (!ui.item) {
                      this.value = '';
                      $('.flavor_line.active .name-val').css('color', 'red');
                      alert('Please make sure you select an existing flavor! Only exisiting flavors may be used.');
                    }
                  else{
                   $('.flavor_line.active .name-val').css('color', '#404040;');
                  }
                }
              })
              .data('ui-autocomplete')._renderItem = function(ul, item) {
                return $('<li></li>')
                  .data("item.autocomplete", item)
                  .append('<a>' + item[_data.item_label] + '</a>')
                  .appendTo(ul);
              };
            // end autocomplete
          });
        });

      })(); // end autocomplete function


    } else {
      console.log("We connected to the server, but it returned an error.");
    }
  };
  ourRequest.onerror = function() {
    console.log("Connection error");
  };
  ourRequest.send();





// jQuery End
  });
});
