<?php
/**
 * Template Name: Recipes
 *
 *
 * @package understrap
 */


get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<style media="screen">
	.recipe-card .card-action .btn-flat {
		font-size: 12px;
		padding: 0 1rem;
		margin-right: 6px !important;
		height: initial;
		line-height: 22px;

	}
</style>

<nav class="sortBar white grey-text text-darken-1">
		<div class="nav-wrapper">
			<a href="#" data-activates="slide-out-filter" class="button-collapse grey-text text-darken-1">Filter Options</a>
			<ul id="mobileSortBar" class="hide-on-large-only">
				<li><?php echo facetwp_display( 'facet', 'recipe_search' ); ?></li>
			</ul>

			</ul>
			<ul id="sortBarNav" class="left hide-on-med-and-down">
				<li><?php echo facetwp_display( 'facet', 'recipe_search' ); ?></li>
				<li class="sortBy"><?php echo facetwp_display( 'sort' ); ?></li>
				<li><?php echo facetwp_display( 'facet', 'flavors' ); ?></li>
				<li><?php echo facetwp_display( 'facet', 'mixologist' ); ?></li>
				<li><a class="grey-text text-darken-1 sortbarNavMobileBtn" onclick="FWP.reset()">Reset</a></li>
			</ul>
			<ul id="slide-out-filter" class="side-nav full sortbarNavMobile">
				<li class="sortBy"><?php echo facetwp_display( 'sort' ); ?></li>
				<li><?php echo facetwp_display( 'facet', 'mflavors' ); ?></li>
				<li><?php echo facetwp_display( 'facet', 'mmixologist' ); ?></li>
				<li><a class="grey-text text-darken-1" onclick="FWP.reset()">Reset</a></li>

			</ul>
		</div>
	</nav>

<div class="wrapper" id="full-width-page-wrapper">


	<div class="" id="content">




			<div class="content-area" id="primary">

				<main class="site-main" style="margin-top: 2rem;" id="main" role="main">

				<!-- Page Layout here -->


<div class="container" id="container">
  <div class="row">




    <div class="col s12 m12 l10 offset-l1">
			<div id="card-container" class="row white z-depth-1">
      	<?php echo facetwp_display( 'template', 'recipes' ); ?>

				<?php echo facetwp_display( 'pager' ); ?>
			</div>
    </div>




				</main><!-- #main -->

			</div><!-- #primary -->



		</div><!-- .row end -->


	</div><!-- Container end -->





</div>
</div>

<div class="fixed-action-btn">
	<a class="btn-floating btn-large red lighten-1">
		<span style="font-size: 34px; font-weight: 100;">+</span>
	</a>
	<ul>
		<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
		<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
		<li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
		<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
	</ul>
</div>

<?php get_footer(); ?>
