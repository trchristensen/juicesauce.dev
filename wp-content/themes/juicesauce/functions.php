<?php
/**
 * Juicesauce functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Juicesauce
 */

if ( ! function_exists( 'juicesauce_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function juicesauce_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Juicesauce, use a find and replace
	 * to change 'juicesauce' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'juicesauce', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'juicesauce' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'juicesauce_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'juicesauce_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function juicesauce_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'juicesauce_content_width', 640 );
}
add_action( 'after_setup_theme', 'juicesauce_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

 class EmojiSupportRemoval
 {
     /**
      * EmojiSupportRemoval constructor.
      */
     public function __construct()
     {
         $this->removeFrontendSupport();
         $this->removeAdminSupport();
     }

     /**
      * Removes theme front-end support.
      */
     private function removeFrontendSupport()
     {
         remove_action('wp_head', 'print_emoji_detection_script', 7);
         remove_action('wp_print_styles', 'print_emoji_styles');
     }

     /**
      * Removes theme admin support.
      */
     private function removeAdminSupport()
     {
         remove_action('admin_print_scripts', 'print_emoji_detection_script');
         remove_action('admin_print_styles', 'print_emoji_styles');
     }
 }

 new EmojiSupportRemoval();

/**
 * Enqueue scripts and styles.
 */

 function enqueue_scripts() {
 	wp_deregister_script( 'jquery' );
 	wp_register_script( 'jquery', ( "//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js" ), false );
 	wp_enqueue_script( 'jquery' );
 	wp_deregister_script( 'jquery-ui' );
 // 	wp_register_script( 'jquery-ui', ( "//code.jquery.com/ui/1.11.4/jquery-ui.min.js" ), false, '1.11.4' );
 // 	wp_enqueue_script( 'jquery-ui' );


 }

 add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

function juicesauce_scripts() {
	wp_enqueue_style( 'juicesauce-style', get_stylesheet_uri() );



	wp_register_style('materialize', get_template_directory_uri() . '/css/materialize.css', array(), '0.9.8', 'all');
	wp_enqueue_style('materialize', array(), null, true); // Enqueue it!

	wp_register_style('autocompletecss', '//cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css');
	wp_enqueue_style('autocompletecss', array(), null, true); // Enqueue it!

	wp_register_style('fontAweseome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '', 'all');
	wp_enqueue_style('fontAweseome', array(), null, true); // Enqueue it!

	wp_enqueue_script( 'juicesauce-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'juicesauce-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// // Extra scriptz
	// $scriptsrc = get_stylesheet_directory_uri().'/js/lib/jquery-2.2.2.min.js';
	// wp_register_script( 'jquery222', $scriptsrc );
	// wp_enqueue_script('jquery222', array(), null, false);

	$scriptsrc = get_stylesheet_directory_uri().'/js/lib/conditionizr-4.3.0.min.js';
	wp_register_script( 'conditionizr', $scriptsrc );
	wp_enqueue_script('conditionizr', array(), null, false);

	$scriptsrc = get_stylesheet_directory_uri().'/js/lib/modernizr-2.7.1.min.js';
	wp_register_script( 'modernizr', $scriptsrc );
	wp_enqueue_script('modernizr', array(), null, false);

	$scriptsrc = get_stylesheet_directory_uri().'/js/lib/materialize.min.js';
	wp_register_script( 'materializejs', $scriptsrc );
	wp_enqueue_script('materializejs', array(), null, false);

	$scriptsrc = get_stylesheet_directory_uri().'/js/lib/lodash.js';
	wp_register_script( 'lodash', $scriptsrc );
	wp_enqueue_script('lodash', array(), null, false);

	$scriptsrc = get_stylesheet_directory_uri().'/js/lib/vue.min.js';
	wp_register_script( 'vuejs', $scriptsrc );
	wp_enqueue_script('vuejs', array(), null, false);
	//
	$scriptsrc1 = '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js';
	wp_register_script( 'jqueryui', $scriptsrc1 );
	wp_enqueue_script('jqueryui', array(), null, false);

	$scriptsrc = get_stylesheet_directory_uri().'/js/lib/jquery.easing.min.js';
	wp_register_script( 'easing', $scriptsrc );
	wp_enqueue_script('easing', array(), null, false);


	// $scriptsrc = '//use.fontawesome.com/ab271d3d95.js';
	// wp_register_script( 'fontawesome', $scriptsrc );
	// wp_enqueue_script('fontawesome', array(), null, false);

	// if( is_page_template( 'template-add-recipe.php' ) ) :
		// Vue Calculator
		$scriptsrc = get_stylesheet_directory_uri().'/js/lib/calculator.js';
		wp_register_script( 'calculator', $scriptsrc );
		wp_enqueue_script('calculator', array(), null, false);
		wp_localize_script('calculator', 'magicalData', array(
			'nonce' => wp_create_nonce('wp_rest'),
			'siteURL' => get_site_url()
		));

		// Add Recipe Script
		$scriptsrc = get_stylesheet_directory_uri().'/js/add-recipe.js';
		wp_register_script( 'add-recipe', $scriptsrc );
		wp_enqueue_script('add-recipe', array(), null, false);

		// Add Scripts.js Script
		$scriptsjs = get_stylesheet_directory_uri().'/js/scripts.js';
		wp_register_script( 'scriptsjs', $scriptsjs );
		wp_enqueue_script('scriptsjs', array(), null, false);


}
add_action( 'wp_enqueue_scripts', 'juicesauce_scripts' );

// Remove Query Strings from scripts (static sources)
function _remove_script_version( $src ){
$parts = explode( '?ver', $src );
return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


add_action( 'wp_enqueue_scripts', 'misha_ajax_comments_scripts' );

function misha_ajax_comments_scripts() {


	// just register for now, we will enqueue it below
	wp_register_script( 'ajax_comment', get_stylesheet_directory_uri() . '/ajax-comment.js', array('jquery') );

	// let's pass ajaxurl here, you can do it directly in JavaScript but sometimes it can cause problems, so better is PHP
	wp_localize_script( 'ajax_comment', 'misha_ajax_comment_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'
	) );

 	wp_enqueue_script( 'ajax_comment' );
}



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';


// Shorten content
function shortenContent($content)
 {
	 // Take the existing content and return a subset of it
	 return substr($content, 0, 300);
 }


 if ( current_user_can( 'administrator' ) ) {
 //Create General Options for Theme
 	if ( function_exists( 'acf_add_options_page' ) ) {
 		acf_add_options_page( array(
 			'page_title' => 'General Options',
 			'menu_title' => 'General Options',
 			'menu_slug'  => 'options',
 			'capability' => 'edit_posts',
 			'redirect'   => false
 		) );

 	}
 }


//Recipes CPT
function Recipes() {

$labels = array(
'name'                  => _x( 'Recipes', 'Post Type Recipes', 'recipes' ),
'singular_name'         => _x( 'Recipe', 'Post Type Singular Name', 'Recipe' ),
'menu_name'             => __( 'Recipes', 'recipes' ),
'name_admin_bar'        => __( 'Recipes', 'recipes' ),
'archives'              => __( 'Recipes Archives', 'recipes' ),
'parent_item_colon'     => __( 'Parent Recipe', 'recipes' ),
'all_items'             => __( 'All Recipes', 'recipes' ),
'add_new_item'          => __( 'Add New Recipe', 'recipes' ),
'add_new'               => __( 'Add Recipe', 'recipes' ),
'new_item'              => __( 'New Recipe', 'recipes' ),
'edit_item'             => __( 'Edit Recipe', 'recipes' ),
'update_item'           => __( 'Update Recipe', 'recipes' ),
'view_item'             => __( 'View Recipe', 'recipes' ),
'search_items'          => __( 'Search Recipes', 'recipes' ),
);
$args = array(
'label'                 => __( 'Recipes', 'Recipes' ),
'description'           => __( 'Recipes', 'recipes' ),
'labels'                => $labels,
'supports' => array(
  'title',
  'thumbnail',
  'comments',
  'editor',
	'revisions'),
'taxonomies'            => array( 'category', 'recipes-tag' ),
'hierarchical'          => true,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'menu_icon'         => 'dashicons-editor-ul',
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => true,
'exclude_from_search'   => false,
'publicly_queryable'    => true,
'capability_type'       => 'page',
'show_in_rest'       => true,
'rest_controller_class' => 'WP_REST_Posts_Controller',
// 'rewrite'               => array( 'slug' => 'recipes' ),

);
register_post_type( 'recipes', $args );

add_rewrite_rule(
		 'recipes/([a-z-]+)/([0-9]+)?$',
		 'index.php?post_type=recipes&name=$matches[1]&p=$matches[2]',
		 'top' );

}

add_action( 'init', 'Recipes', 0 );



function change_post_type_link( $link, $post = 0 ){
    if ( $post->post_type == 'recipes' ){
        return home_url( 'recipes/'. $post->post_name .'/'. $post->ID );
    } else {
        return $link;
    }
}


add_filter('post_type_link', 'change_post_type_link', 10, 2);







//Flavors CPT
function Flavors() {

$labels = array(
'name'                  => _x( 'Flavors', 'Post Type Flavors', 'flavors' ),
'singular_name'         => _x( 'Flavor', 'Post Type Singular Name', 'Flavor' ),
'menu_name'             => __( 'Flavors', 'flavors' ),
'name_admin_bar'        => __( 'Flavors', 'flavors' ),
'archives'              => __( 'Flavors Archives', 'flavors' ),
'parent_item_colon'     => __( 'Parent Flavor', 'flavors' ),
'all_items'             => __( 'All Flavors', 'flavors' ),
'add_new_item'          => __( 'Add New Flavor', 'flavors' ),
'add_new'               => __( 'Add Flavor', 'flavors' ),
'new_item'              => __( 'New Flavor', 'flavors' ),
'edit_item'             => __( 'Edit Flavor', 'flavors' ),
'update_item'           => __( 'Update Flavor', 'flavors' ),
'view_item'             => __( 'View Flavor', 'flavors' ),
'search_items'          => __( 'Search Flavors', 'flavors' ),
);
$args = array(
'label'                 => __( 'Flavors', 'Flavors' ),
'description'           => __( 'Flavors', 'flavors' ),
'labels'                => $labels,
'supports' => array(
  'title',
  'thumbnail',
  'comments',
  'editor'),
'taxonomies'            => array( 'category', 'flavors-tag' ),
'hierarchical'          => false,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'menu_icon'         => 'dashicons-editor-ul',
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => true,
'exclude_from_search'   => true,
'publicly_queryable'    => true,
'capability_type'       => 'page',
'show_in_rest'       => true,
'rest_controller_class' => 'WP_REST_Posts_Controller',
);
register_post_type( 'flavors', $args );

}

add_action( 'init', 'Flavors', 0 );



//Flavors CPT
function Stash() {

$labels = array(
'name'                  => _x( 'Stash', 'Post Type Stash', 'stash' ),
'singular_name'         => _x( 'Stash', 'Post Type Singular Name', 'Stash' ),
'menu_name'             => __( 'Stash', 'stash' ),
'name_admin_bar'        => __( 'Stash', 'stash' ),
'archives'              => __( 'Stash Archives', 'stash' ),
'parent_item_colon'     => __( 'Parent Stash', 'stash' ),
'all_items'             => __( 'Stash', 'stash' ),
'add_new_item'          => __( 'Add New Flavor', 'stash' ),
'add_new'               => __( 'Add to Stash', 'stash' ),
'new_item'              => __( 'New Flavor', 'stash' ),
'edit_item'             => __( 'Edit Flavor', 'stash' ),
'update_item'           => __( 'Update Flavor', 'stash' ),
'view_item'             => __( 'View Flavor', 'stash' ),
'search_items'          => __( 'Search Stash', 'stash' ),
);
$args = array(
'label'                 => __( 'Stash', 'Stash' ),
'description'           => __( 'Stash', 'stash' ),
'labels'                => $labels,
'supports' => array(
  'title',
  'thumbnail',
  'comments',
  'editor',
	'author'),
'taxonomies'            => array( 'category', 'stash-tag' ),
'hierarchical'          => false,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'menu_icon'         => 'dashicons-editor-ul',
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => false,
'exclude_from_search'   => true,
'publicly_queryable'    => true,
'capability_type'       => 'page',
'show_in_rest'       => false,
// 'rest_controller_class' => 'WP_REST_Posts_Controller',
);
register_post_type( 'stash', $args );

}

add_action( 'init', 'Stash', 0 );


/* This example syncs both UM / WP role during user approval */

add_action('um_after_user_is_approved', 'sync_um_and_wp_role', 99 );
function sync_um_and_wp_role( $user_id ) {
$role = get_user_meta( $user_id, 'role', true);
	if ( $role == 'member' ) {
	$wp_user_object = new WP_User( $user_id );
	$wp_user_object->set_role( 'member' );
	}
}




/* First we need to extend main profile tabs */

add_filter('um_profile_tabs', 'add_custom_profile_tab', 1000 );
function add_custom_profile_tab( $tabs ) {

	$tabs['recipes'] = array(
		'name' => 'Recipes',
		'icon' => 'um-icon-ios-list',
	);

	return $tabs;

}

/* Then we just have to add content to that tab using this action */

add_action('um_profile_content_recipes_default', 'um_profile_content_recipes_default');

function um_profile_content_recipes_default( $args ) {

echo '<h1>Recipes</h1>';

$profile_id = um_profile_id();


// the query
$wpb_all_query = new WP_Query(array(
	'post_type' => 'recipes',
	'posts_per_page' => -1,
	'author__in' => $profile_id,
	 'query_args' => array(
			'orderby' => 'meta_value_num', // sort by numerical custom field
			'meta_key' => 'epicredrank', // required when sorting by custom fields
			'order' => 'DESC', // descending order
		),
));

 if ( $wpb_all_query->have_posts() ) : ?>



	<!-- the loop -->
	<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

		<?php
		// Variables
		// vg %
		$vgP = get_field('vg_ratio');
		// pg %
		$pgP = get_field('pg_ratio');
		// nicotine strength
		$nicBaseMg = get_field('nicBaseMg');
		// nicotine mg
		$nicMgMl = get_field('nic_mgml');
		// nicotine vg
		$nicVg = get_field('nicBaseVg');
		// nicotine pg
		$nicPg = get_field('nicBasePg');

		$content = get_the_content();
		?>

		        <!-- Col: Card 1 -->
		        <div class="col s12 m12 l12">
		          <!-- Card 1 -->
		          <div class="card recipe-card">

		            <div class="card-content">

		              <span class="card-title grey-text text-darken-4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
		                <?php um_fetch_user( get_the_author_meta('ID')); ?>
		                <p class="card-subtitle grey-text text-darken-2"><a href="<?php echo um_user_profile_url() ?>"><?php echo esc_attr(um_user('display_name')); ?></a></p>

		              <p class="card-subtitle grey-text text-darken-2"><?php echo shortenContent($content); ?></p>

									<div class="status-bar">
										<?php if(function_exists('wp_ulike')) wp_ulike('get'); ?>
										<div class="comment-box">
										<div class="comment-box-content">
										<i class="fa fa-comments" aria-hidden="true"></i>
										<?php comments_number('0', '1', '%'); ?>
										</div>

										</div>
										<div class="fork-box">
										  <div class="fork-box-content">
										    <i class="fa fa-code-fork" aria-hidden="true"></i>
										<span><?php echo count(get_field('recipeRevisions')); ?></span>
										  </div>

										</div>
									</div>


		            </div>
		            <div class="card-action">
		              <div class="col-left">
		                <?php while ( have_rows('recipeFlavors') ) : the_row(); ?>
		                      <?php $post_object = get_sub_field('recipeFlavor'); ?>
		                      <?php if( $post_object ): ?>
		                          <?php $post = $post_object; setup_postdata( $post ); ?>
		                           <?php $post_slug = $post->post_name; ?>
		                          <a href="<?php the_permalink(); ?>" class=" <?php echo $post_slug ?>"><?php echo $post_slug ?></a>
		                          <?php wp_reset_postdata(); ?>
		                      <?php endif; ?>
		                <?php endwhile; ?>
		              </div>
		              <div class="col-right">

		              </div>



		            </div>
		          </div>
		          <!-- End of card -->
		        </div>
		        <!-- End of col -->

	<?php endwhile; ?>
	<!-- end of the loop -->



	<?php wp_reset_postdata(); ?>

<?php else : ?>

<?php endif; ?>



<?php }



// add_filter( 'facetwp_sort_options', 'my_facetwp_sort_by_popularity', 10, 2 );


// SORT OPTIONS - ORDER BY Vote Popularity
function my_facetwp_sort_popularity( $options, $params ) {
		$options['sort_by_popularity'] = array(
				'label' => 'Popularity',
				'query_args' => array(
						'orderby' => 'meta_value_num', // sort by numerical custom field
						'meta_key' => 'epicredrank', // required when sorting by custom fields
						'order' => 'DESC', // descending order
				)
		);
		return $options;
}

function trashPost() {
	$recipeID = $_POST['recipeID'];
	// $ogRecipe = $_POST['ogRecipe'];

	// delete_field('field_58d9094576888', $ogRecipe );
	wp_delete_post( $recipeID );

}

add_action( 'wp_ajax_nopriv_trashPost',  'trashPost' );
add_action( 'wp_ajax_trashPost','trashPost' );


// Function to add http:// to url string
function addScheme($url, $scheme = 'http://')
{
  return parse_url($url, PHP_URL_SCHEME) === null ?
    $scheme . $url : $url;
}


//Sup from ajax bro
//function to make that JUICE
 function makeJuice() {
        //lets get that POST from our AJAX
        $userID = $_POST['userID'];
        $recipeName = $_POST['recipeName'];
        $recipeNotes = $_POST['recipeNotes'];
				$sourceURL = $_POST['sourceURL'];
        $postType = "recipes"; //the post type!

				$nicBaseMg = $_POST['fields']['nicBaseMg'];
				$nicBaseVg = $_POST['fields']['nicBaseVg'];
				$nicBasePg = $_POST['fields']['nicBasePg'];
				$bottle_ml = $_POST['fields']['bottle_ml'];
				$nic_mgml = $_POST['fields']['nic_mgml'];
				$vg_ratio = $_POST['fields']['vg_ratio'];
				$pg_ratio = $_POST['fields']['pg_ratio'];
				$vg_drops = $_POST['fields']['vg_drops'];
				$vg_ml = $_POST['fields']['vg_ml'];
				$vg_g = $_POST['fields']['vg_g'];
				$pg_drops = $_POST['fields']['pg_drops'];
				$pg_ml = $_POST['fields']['pg_ml'];
				$pg_g = $_POST['fields']['pg_g'];
				$nic_drops = $_POST['fields']['nic_drops'];
				$nic_ml = $_POST['fields']['nic_ml'];
				$nic_g = $_POST['fields']['nic_g'];
				$recipeFlavors = $_POST['fields']['recipeFlavors'];

				$originalRecipe = $_POST['fields']['original_recipe'];
				if($originalRecipe == 0) {
					$originalRecipe = null;
				}

				// make sure the source url is structured correctly.
				if(!empty($sourceURL)) {
					$sourceURL = addScheme($sourceURL);
				}


        //then we create a post
       // but 1st lets take a selfy... I mean check if the post doesn't already exist, then create it
			if( null == get_page_by_title(  $recipeName ) ) {
			    // Set the postID so that we know the post was created successfully
			    //and add ACFs

			    $postID = wp_insert_post(array (
			        'post_type'             => $postType,
			        'post_title'              => $recipeName,  // post title will be recipe name
			        'post_content'       => $recipeNotes,  // you always needs this even if you don't use it just add some bs in there for now is recript notes
			        'post_status'         => 'publish',  //publish for now or draft if you want up to you
			        'post_author'		  => $userID,  // author ID aka user ID
							'post_parent'			=> $originalRecipe,


			    ));

			//here is how you would then add an acf
			// using the  update_field('nameOfField', $variableWithContent, $postID);
			update_field('field_58d102e479886', $nicBaseMg, $postID);
			update_field('field_58d1032279887', $nicBaseVg, $postID);
			update_field('field_58d1033079888', $nicBasePg, $postID);
			update_field('field_58d1034179889', $bottle_ml, $postID);
			update_field('field_58d103497988a', $nic_mgml, $postID);
			update_field('field_58d103537988b', $vg_ratio, $postID);
			update_field('field_58d1035e7988c', $pg_ratio, $postID);
			update_field('field_58db1a71df367', $vg_drops, $postID);
			update_field('field_58db1a9cdf36a', $vg_ml, $postID);
			update_field('field_58d1032279887', $vg_g, $postID);
			update_field('field_58db1a81df368', $pg_drops, $postID);
			update_field('field_58db1aacdf36b', $pg_ml, $postID);
			update_field('field_58db1ad1df36e', $pg_g, $postID);
			update_field('field_58db1a8bdf369', $nic_drops, $postID);
			update_field('field_58db1ab4df36c', $nic_ml, $postID);
			update_field('field_58db1adadf36f', $nic_g, $postID);
			update_field('field_59258859854eb', $sourceURL, $postID);

			update_field('field_58d4149773ca1', $recipeFlavors, $postID);

			//initializing likes meta so it shows up in queries even with no likes
			update_post_meta( $postID, '_liked', 0 );

			// Special Revision Field
			// if(!empty($originalRecipe)) {
				// update_field('field_58d9094576888', $originalRecipe, $postID);
			//
			//
			// 	// Now let's update the original post by adding the revision id to it.
			// 	// adding the current revision post to a revision list on the original recipe
			// 	$recipeRevisions = get_field('recipeRevisions', $originalRecipe);
			// 	$thisPostID = $postID;
			//
			// 	if( !is_array($recipeRevisions) ):
			// 		$recipeRevisions = array();
			// 	endif;
			//
			// 	array_push($recipeRevisions, $thisPostID);
			// 	update_field('field_591a8163febc3', $recipeRevisions, $originalRecipe);
			// }





			}
		//echo the id so ajax can redirect to the page once its done.
	echo get_post_permalink($postID);

 	}
	 //and then you do some wordpress magic
	 add_action( 'wp_ajax_nopriv_makeJuice',  'makeJuice' );
	 add_action( 'wp_ajax_makeJuice','makeJuice' );
	 //and DONE! <img src="makeitrain.gif" />



// New function to UPDATE juice

//Sup from ajax bro
//function to make that JUICE
 function updateJuice() {
        //lets get that POST from our AJAX
				$recipeID = $_POST['recipeID'];

        $userID = $_POST['userID'];
        $recipeName = $_POST['recipeName'];
        $recipeNotes = $_POST['recipeNotes'];
				$sourceURL = $_POST['sourceURL'];
        $postType = "recipes"; //the post type!

				$nicBaseMg = $_POST['fields']['nicBaseMg'];
				$nicBaseVg = $_POST['fields']['nicBaseVg'];
				$nicBasePg = $_POST['fields']['nicBasePg'];
				$bottle_ml = $_POST['fields']['bottle_ml'];
				$nic_mgml = $_POST['fields']['nic_mgml'];
				$vg_ratio = $_POST['fields']['vg_ratio'];
				$pg_ratio = $_POST['fields']['pg_ratio'];
				$vg_drops = $_POST['fields']['vg_drops'];
				$vg_ml = $_POST['fields']['vg_ml'];
				$vg_g = $_POST['fields']['vg_g'];
				$pg_drops = $_POST['fields']['pg_drops'];
				$pg_ml = $_POST['fields']['pg_ml'];
				$pg_g = $_POST['fields']['pg_g'];
				$nic_drops = $_POST['fields']['nic_drops'];
				$nic_ml = $_POST['fields']['nic_ml'];
				$nic_g = $_POST['fields']['nic_g'];
				$recipeFlavors = $_POST['fields']['recipeFlavors'];

        //then we create a post
       // but 1st lets take a selfy... I mean check if the post doesn't already exist, then create it
			// if( null == get_page_by_title(  $recipeName ) ) {
			    // Set the postID so that we know the post was created successfully
			    //and add ACFs

			    $postID = wp_update_post(array (
							'ID'							   => $recipeID, //recipe id
			        'post_type'          => $postType,
			        'post_title'         => $recipeName,  // post title will be recipe name
			        'post_content'       => $recipeNotes,  // you always needs this even if you don't use it just add some bs in there for now is recript notes
			        'post_status'        => 'publish',  //publish for now or draft if you want up to you
			        'post_author'		  	 => $userID,  // author ID aka user ID

			    ));

			//here is how you would then add an acf
			// using the  update_field('nameOfField', $variableWithContent, $postID);
			update_field('field_58d102e479886', $nicBaseMg, $postID);
			update_field('field_58d1032279887', $nicBaseVg, $postID);
			update_field('field_58d1033079888', $nicBasePg, $postID);
			update_field('field_58d1034179889', $bottle_ml, $postID);
			update_field('field_58d103497988a', $nic_mgml, $postID);
			update_field('field_58d103537988b', $vg_ratio, $postID);
			update_field('field_58d1035e7988c', $pg_ratio, $postID);
			update_field('field_58db1a71df367', $vg_drops, $postID);
			update_field('field_58d1032279887', $vg_ml, $postID);
			update_field('field_58d1032279887', $vg_g, $postID);
			update_field('field_58db1a81df368', $pg_drops, $postID);
			update_field('field_58db1aacdf36b', $pg_ml, $postID);
			update_field('field_58db1ad1df36e', $pg_g, $postID);
			update_field('field_58db1a8bdf369', $nic_drops, $postID);
			update_field('field_58db1ab4df36c', $nic_ml, $postID);
			update_field('field_58db1adadf36f', $nic_g, $postID);
			update_field('field_59258859854eb', $sourceURL, $postID);

			update_field('field_58d4149773ca1', $recipeFlavors, $postID);

			// }
		//echo the id so ajax can redirect to the page once its done.
	echo get_permalink($postID);
 	}
	 //and then you do some wordpress magic
	 add_action( 'wp_ajax_nopriv_updateJuice',  'updateJuice' );
	 add_action( 'wp_ajax_updateJuice','updateJuice' );
	 //and DONE! <img src="makeitrain.gif" />


// Rest API support for revisions
	function acf_revision_support($types)
	{
	    $types['revision'] = 'revision';

	    return $types;
	}
	add_filter('acf/rest_api/types', 'acf_revision_support');

// takes the url and returns the main path (host)
function sourceRoot($input) {
	// in case scheme relative URI is passed, e.g., //www.google.com/
		$input = trim($input, '/');

		// If scheme not included, prepend it
		if (!preg_match('#^http(s)?://#', $input)) {
		    $input = 'http://' . $input;
		}

		$urlParts = parse_url($input);

		// remove www
		$domain = preg_replace('/^www\./', '', $urlParts['host']);

		echo $domain;

}



// FACETWP ORDER BY FUNCTIONS

add_filter( 'facetwp_sort_options', function( $options, $params ) {
    $options['num_likes'] = array(
        'label' => 'Most Likes',
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => '_liked',
            'order' => 'DESC',
        )
    );
    return $options;
}, 10, 2 );


// FACETWP ORDER BY FUNCTIONS

add_filter( 'facetwp_sort_options', function( $options, $params ) {
    $options['likes_popularity'] = array(
        'label' => 'Most Recent Likes',
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => '_liked',
            'order' => 'DESC',
        ),
				'date_query' => array(
                     array(
        'after'     => '2 days ago',
        'inclusive' => true,
       ),
     ),
    );
    return $options;
}, 10, 2 );



add_filter( 'facetwp_sort_options', function( $options, $params ) {
    $options['comments_popularity'] = array(
        'label' => 'Most Recent Comments',
        'query_args' => array(
            'orderby' => 'comment_count',
            'order' => 'DESC',
        ),
				'date_query' => array(
                     array(
        'after'     => '2 days ago',
        'inclusive' => true,
       ),
     ),
    );
    return $options;
}, 10, 2 );

add_filter( 'facetwp_sort_options', function( $options, $params ) {
    $options['comments_num'] = array(
        'label' => 'Most Comments',
        'query_args' => array(
            'orderby' => 'comment_count',
            'order' => 'DESC',
        ),
    );
    return $options;
}, 10, 2 );


function comicpress_copyright() {
	global $wpdb;
	$copyright_dates = $wpdb->get_results("
	SELECT
	YEAR(min(post_date_gmt)) AS firstdate,
	YEAR(max(post_date_gmt)) AS lastdate
	FROM
	$wpdb->posts
	WHERE
	post_status = 'publish'
	");
	$output = '';
	if($copyright_dates) {
	$copyright = "&copy; " . $copyright_dates[0]->firstdate;
	if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
	$copyright .= '-' . $copyright_dates[0]->lastdate;
	}
	$output = $copyright;
	}
	return $output;
}
