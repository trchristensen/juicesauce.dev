<?php
/**
 * Template Name: Ninja
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
?>
<?php
if ( !is_user_logged_in() ) {
   auth_redirect();
}

$user = wp_get_current_user();
$allowed_roles = array('editor', 'administrator', 'author', 'member', 'subscriber');
if( array_intersect($allowed_roles, $user->roles ) ) {

?>

<?php wp_head(); ?>

  <?php get_header(); ?>

  <div class="container" id="wrapper">


    <div class="components">

      <div class="readable no-print row">
        <div class="col s12 m12 panel topPanel">

            <!-- Edit / Printable -->
            <div class="switch">
              <label>
                   Edit
                   <input v-model="printable" type="checkbox">
                   <span class="lever"></span>

                 </label>
            </div>


        </div>

      </div>

      <div v-show="!printable" id="base-components" class="row boxee">


        <div class="row">
          <div class="input-field col s12">
            <i class="material-icons prefix">mode_edit</i>
            <input id="recipeName" type="text" class="validate">
            <label for="recipeName">Recipe Name</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">mode_edit</i>
            <textarea id="recipeNotes" class="materialize-textarea"></textarea>
            <label for="recipeNotes">Recipe Notes</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">mode_edit</i>
            <input id="sourceURL" type="text" class="validate">
            <label for="sourceURL">Source URL</label>
          </div>

          <!-- <div class="input-field col s12 categories-select">
            <i class="material-icons prefix">label_outline</i>
            <select multiple class="categories">
              <option value="" disabled selected>Recipe Categories</option>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </select>
            <label>Recipe Categories</label>
          </div> -->

        </div>

        <div class="row">
          <div class="section-heading">
            <h5 class="left-align">Nicotine Base</h5>
          </div>
          <div class="col s12 m4">
            <div class="input-field">
              <input v-model="nicBaseMg" id="nicBaseMg" type="number" min="0" class="validate" number>
              <label for="nicBaseMg">Nicotine MG/ML</label>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="input-field">
              <input v-model="nicVG" id="nicBaseVg" @change="changeNicRatio('VG')" type="number" min="0" max="100" class="validate" number>
              <label for="nicBaseVg">VG ratio %</label>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="input-field">
              <input v-model="nicPG" id="nicBasePg" @change="changeNicRatio('PG')" type="number" min="0" max="100" class="validate" number>
              <label for="nicBasePg">PG ratio %</label>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="section-heading">
            <h5 class="left-align">Target Measurements</h5>
          </div>
          <div class="col s6">
            <div class="input-field">
              <input id="bottle_ml" type="number" min="1" v-model="targetBottleSize" class="validate" number>
              <label for="bottle_ml">Bottle Size (ML)</label>
            </div>
          </div>

          <div class="col s6">
            <div class="input-field">
              <input v-model="targetNicMg" id="nic_mgml" type="number" min="0" max="{{nicBaseMg}}" class="" number>
              <label for="nic_mgml">Nicotine (mg/ml)</label>
            </div>
          </div>

          <div class="col s6">
            <div class="input-field">
              <input v-model="targetRatioVg" id="vg_ratio" type="number" min="0" @change="changeVgPg" max="100" class="vgpgratio validate" number>
              <label for="vg_ratio">VG Ratio (%)</label>
            </div>
          </div>

          <div class="col s6">
            <div class="input-field">
              <input v-model="targetRatioPg" id="pg_ratio" type="number" min="0" @change="changeVgPg" max="100" class="vgpgratio validate" number>
              <label for="pg_ratio">PG Ratio (%)</label>
            </div>
          </div>
        </div>

      </div>




      <!-- Ingredients -->
      <ul class="collection row boxee" id="recipe-totals">

        <li v-if="printable" class="collection-item ingredient-header-line">
          <div class="row ingredient-header-line">
            <div class="col s3">
              <p class="ing-heading">Bottle Size:<br /> {{targetBottleSize}}&nbsp;ML</p>
            </div>
            <div class="col s3">
              <p class="ing-heading">VG/PG Ratio:<br /> {{targetRatioVg}}/{{targetRatioPg}}</p>
            </div>
            <div class="col s6">
              <p class="ing-heading">Target Nicotine:<br /> {{targetNicMg}}&nbsp;MG - <br class="hide-on-med-and-up" /> ({{nicVG}}/{{nicPG}} {{nicBaseMg}}MG Base)</p>
            </div>
          </div>
        </li>

        <li class="collection-item ingredient-header-line">
          <div class="row ingredient-header-line">
            <div class="col s3">
              <p class="ing-heading">Ingredient</p>
            </div>
            <div class="col s3">
              <p class="ing-heading">drops</p>
            </div>
            <div class="col s3">
              <p class="ing-heading">milliliters</p>
            </div>
            <div class="col s3">
              <p class="ing-heading">grams</p>
            </div>

          </div>
        </li>
        <li class="collection-item" id="vg_line">
          <div class="row ingredient-line">
            <div class="col s3">
              <p class="name-val">VG</p>
            </div>
            <div class="col s3">
              <p class="name-val drops">{{leftoverVgMl | ml_to_drops | roundl 0}}</p>
            </div>
            <div class="col s3">
              <p class="name-val ml">{{leftoverVgMl | roundl}}</p>
            </div>
            <div class="col s3">
              <p class="name-val grams">{{leftoverVgMl | ml_to_grams 'VG' | roundl}}</p>
            </div>

          </div>
        </li>
        <li class="collection-item" id="pg_line">
          <div class="row ingredient-line">
            <div class="col s3">
              <p class="name-val">PG</p>
            </div>
            <div class="col s3">
              <p class="name-val drops">{{leftoverPgMl | ml_to_drops | roundl 0}}</p>
            </div>
            <div class="col s3">
              <p class="name-val ml">{{leftoverPgMl | roundl}}</p>
            </div>
            <div class="col s3">
              <p class="name-val grams">{{leftoverPgMl | ml_to_grams 'PG' | roundl}}</p>
            </div>

          </div>
        </li>
        <li class="collection-item" id="nic_line">
          <div class="row ingredient-line">
            <div class="col s3">
              <p class="name-val">Nicotine</p>
            </div>
            <div class="col s3">
              <p class="name-val drops">{{ nicPercentage | percent_to_ml | ml_to_drops | roundl 0 }}</p>
            </div>
            <div class="col s3">
              <p class="name-val ml">{{ nicPercentage | percent_to_ml | roundl }}</p>
            </div>
            <div class="col s3">
              <p class="name-val grams nic-grams">{{ nicPercentage | percent_to_ml | ml_to_grams "NIC" | roundl }}</p>
            </div>

          </div>
        </li>

        <li v-if="printable" v-for="flavor in flavors" class="collection-item">
          <div class="row ingredient-line">
            <div class="col s3">
              <p class="name-val">{{flavor.name}} - {{flavor.percentage | roundl 4 }}%</p>
            </div>
            <div class="col s3">
              <p class="name-val drops">{{ flavor.percentage | percent_to_ml | ml_to_drops | roundl 0 }}</p>
            </div>
            <div class="col s3">
              <p class="name-val ml">{{ flavor.percentage | percent_to_ml | roundl }}</p>
            </div>
            <div class="col s3">
              <p class="name-val grams">{{ flavor.percentage | percent_to_ml | ml_to_grams flavor.weight_src flavor.custom_weight | roundl }}</p>
            </div>

          </div>
        </li>


      </ul>

      <ul v-show="!printable" class="collapsible row boxee" id="flavors" data-collapsible="accordion">
        <li class="header">
          <div class="collapsible-header">
            <div class="row ingredient-header-line">
              <div class="col s3">
                <p class="ing-heading">Flavor Name</p>
              </div>
              <div class="col s3">
                <p class="ing-heading">drops</p>
              </div>
              <div class="col s3">
                <p class="ing-heading">milliliters</p>
              </div>
              <div class="col s3">
                <p class="ing-heading">grams</p>
              </div>

            </div>
          </div>
        </li>

        <!-- v-loop start -->

        <li v-for="flavor in flavors" class="flavor_line">
          <div class="collapsible-header">
            <div class="row ingredient-line">
              <div class="col s3 name">

                <p class="name-val">{{flavor.name}} - {{flavor.percentage | roundl 4 }}%</p>
              </div>
              <div class="col s3 drops"><p>{{ flavor.percentage | percent_to_ml | ml_to_drops | roundl 0 }}</p></div>
              <div class="col s3 ml"><p>{{ flavor.percentage | percent_to_ml | roundl }}</p></div>
              <!-- <div class="col s3 grams">{{ ml_to_mg( percent_to_ml(flavor.percentage), flavor.weight_src, flavor.custom_weight ) }}</div> -->
              <div class="col s3 grams">
                <p><span class="grams">{{ flavor.percentage | percent_to_ml | ml_to_grams flavor.weight_src flavor.custom_weight | roundl }}</span>
                  <i class="material-icons closed right hide-on-small-only">settings</i>
                </p>

              </div>



            </div>
          </div>
          <div class="collapsible-body">
            <div class="ingredient-body row">
              <div class="col s12">

                <div class="input-field col s12 m6">
                  <i class="material-icons prefix">textsms</i>
                  <label for="flavor-{{flavor.id}}" class="control-label">Flavor Name</label>
                  <input class="form-control bs-autocomplete autocomplete-flavors" id="flavor-{{flavor.id}}" v-model="flavor.name" value="" type="text" data-source="" data-hidden_field_id="flavor-id-{{flavor.id}}" data-item_id="id" data-item_label="label" autocomplete="off">
                  <input class="form-control hidden-flavor-id" id="flavor-id-{{flavor.id}}" name="flavorid" value="" type="hidden" readonly>
                </div>








                <div class="input-field col s12 m6">
                  <input id="flavor-percent-{{flavor.id}}" v-model="flavor.percentage" type="number" min="0" class="flavor_percentage" number>
                  <label for="flavor-percent-{{flavor.id}}" class="active">Percentage</label>
                </div>

                <!-- <div class="input-field col s12 m4">
                  <select v-model="flavor.weight_src" class="browser-default">
                    <option value="PG">PG Weight</option>
                    <option value="VG">VG Weight</option>
                    <option value="c">Custom</option>
                  </select>

                </div> -->

                <!-- <div class="input-field col s12 m6" v-show="flavor.weight_src === 'c'">
                  <input id="flavor-weight-{{flavor.id}}" v-model="flavor.custom_weight" type="number" min="0" class="" number>
                  <label for="flavor-weight-{{flavor.id}}" class="active">Custom Weight (Grams per ML)</label>
                </div> -->

                <div class="col s12">
                  <a class="waves-effect waves-light btn" @click="removeFlavor(flavor.id)">Delete</a>
                  <a class="waves-effect waves-light btn flavor-close right">Close</a>
                </div>
              </div>
            </div>
          </div>
        </li>

      </ul>

      <div v-show="!printable" class="row">
        <div class="col m4 s6">
          <a class="waves-effect waves-light btn" id="addFlavor" @click="addFlavor">Add Flavor</a>
        </div>
      </div>

      <div class="row center-align">
          <a class="waves-effect waves-light btn" id="submitRecipe">Submit Recipe</a>
      </div>





    </div>
  </div>

<script type="text/javascript">
$(document).ready(function() {
  $('select').material_select();
});
</script>

  <?php
    //Here is the start of the magic code aka how I pick up girls "is yo daddy a baker cuz you got buns hun"
 //get the motherfucking user ID
  $user = wp_get_current_user();
  $userID = $user->ID;
  //so here you usually close it down to some roles but we going all out here so fuck this you already have it that only signed up people can see this anyways
 // $allowed_roles = array('editor', 'administrator', 'author');
 // if( array_intersect($allowed_roles, $user->roles ) ) {
//lets get this mother fucking scrip going ya?
 ?>

<script>
$(document).ready(function () {
//first we detect when the form gets submited
$( "#submitRecipe" ).click(function( e ) {



  // Check if values are filled out
  if ($.trim($("#recipeName, #nicBaseMg, #nicBaseVg, #nicBasePg, #bottle_ml, #nic_mgml, #vg_ratio, #pg_ratio").val()) === "") {

      alert('you did not fill out one of the fields!');
  } else {



//we prevent from doing the default aka refreshing the page and send it via post to w/e page
  e.preventDefault();
//then we get the values from the form fields
var recipeName = $("#recipeName").val();
var recipeNotes = $("#recipeNotes").val();
var sourceURL = $("#sourceURL").val();
//since I am a bit fucking drunk I am going to do 2 you can do the rest, I believe in you bruh

// Custom Fields
  var nicBaseMg = $('#nicBaseMg').val();
  var nicBaseVg = $('#nicBaseVg').val();
  var nicBasePg = $('#nicBasePg').val();
  var bottle_ml = $('#bottle_ml').val();
  var nic_mgml = $('#nic_mgml').val();
  var vg_ratio = $('#vg_ratio').val();
  var pg_ratio = $('#pg_ratio').val();
  var vg_drops = $('#vg_line p.name-val.drops').text();
  var vg_ml = $('#vg_line p.name-val.ml').text();
  var vg_g = $('#vg_line p.name-val.grams').text();
  var pg_drops = $('#pg_line p.name-val.drops').text();
  var pg_ml = $('#pg_line p.name-val.ml').text();
  var pg_g = $('#pg_line p.name-val.grams').text();
  var nic_drops = $('#nic_line p.name-val.drops').text();
  var nic_ml = $('#nic_line p.name-val.ml').text();
  var nic_g = $('#nic_line p.name-val.grams').text();

  // Repeater
  var recipeFlavors = [];

  // New loop to get all info for each flavor instead of just the flavor
    $( ".flavor_line" ).each(function( index ) {
      var fDrops = $(this).find(".drops p").text();
      var fMl = $(this).find(".ml p").text();
      var fG = $(this).find(".grams .grams").text();
      var rId = $(this).find("input.hidden-flavor-id").val();
      var flavor_perc = $(this).find(".flavor_percentage").val();

      // Push recipeFlavor object onto recipeFlavors
      recipeFlavors.push({recipeFlavor:{id: rId},flavor_drops: fDrops,flavor_ml: fMl,flavor_g: fG, flavor_perc: flavor_perc});
    });


//Then we get the WordPress Ajax URL
var ajaxurl = 'http://'+window.location.host+'/wp-admin/admin-ajax.php';
// this mother fucker will help up send all the data and use a function in function.php to make a post
    //Ajax thing
    $.ajax({
                    //we just doing normal ajax from wordpress and an action meaning a function in functions.php
                    url: ajaxurl + "?action=makeJuice",
                    //here is the info we should be sending aka content  and crazy advanced custom fields
                    //so in the example below is pretty easy recipeName: is the name of the post and the next variable is the date so is  namePost : data
                    /**
                     *  Example is recipeName : recipeName - the 1st recipeName is the variable that you are passing to functions.php and the next one is the variable that you
                     * made above getting the data from JQuery so each time you want to add an extra variable/form field you get the value of it via jquery and then send it to
                     * functions.php via ajax in the data array
                     *  also you could add all this into one array and just foreach on functions.php if you send all this as a big json object
                     * */
                    // the last one is userID we need that so we can create the post and assign this as the author
                    data: {recipeName: recipeName, recipeNotes: recipeNotes, sourceURL: sourceURL, userID: <?php echo $userID; ?>, fields: {nicBaseMg: nicBaseMg, nicBaseVg: nicBaseVg, nicBasePg: nicBasePg, bottle_ml: bottle_ml, nic_mgml: nic_mgml, vg_ratio: vg_ratio, pg_ratio: pg_ratio, vg_drops: vg_drops, vg_ml: vg_ml, vg_g: vg_g, pg_drops: pg_drops, pg_ml: pg_ml, pg_g: pg_g, nic_drops: nic_drops, nic_ml: nic_ml, nic_g: nic_g, recipeFlavors: recipeFlavors},},
                    //type of request, usually post
                    type: 'POST',
                    success: function(data) {
                    // window.location.href = '/recipes/'.data;
                    // window.location.href = '/' + data;
                    window.location.href = data;
                    console.log(data);
                    },
                    error: function(xhr, ajaxOptions, thrownerror) { }
            }); //ajax function
            }; //End check if values are filled out
    }); //submit function

}); //doc ready
//donezo cya in functions.php
</script>


<?php get_footer(); ?>

<?php } ?> <!--if current user-->
