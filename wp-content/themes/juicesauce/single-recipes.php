<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Juicesauce
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


		<?php
		while ( have_posts() ) : the_post();

			// get_template_part( 'template-parts/content-recipes', get_post_format() );
?>
			<!-- Collect the Variables -->

			<?php
				$recipeName = get_the_title();
				$recipeNotes = get_the_content();
				$nicBaseMg = get_field('nicBaseMg');
				$nicvgRatio = get_field('nicBaseVg');
				$nicpgRatio = get_field('nicBasePg');
				$bottleMl = get_field('bottle_ml');
				$nicotineStrength = get_field('nic_mgml');
				$vgRatio = get_field('vg_ratio');
				$pgRatio = get_field('pg_ratio');
				$vgDrops = get_field('vg_drops');
				$vgMl = get_field('vg_ml');
				$vgG = get_field('vg_g');
				$pgDrops = get_field('pg_drops');
				$pgMl = get_field('pg_ml');
				$pgG = get_field('pg_g');
				$nicDrops = get_field('nic_drops');
				$nicMl = get_field('nic_ml');
				$nicG = get_field('nic_g');


				// Array of Flavors
				$recipeFlavors = get_field('recipeFlavors');

			 ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


				<header class="entry-header" style="text-indent:-999999px;left:-99999px;position:absolute;">
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;

					if ( 'recipes' === get_post_type() ) : ?>
					<div class="entry-meta">

					</div><!-- .entry-meta -->
					<?php
					endif; ?>
				</header><!-- .entry-header -->



						<div class="row">
							<div class="col l12 m12">
								<div class="components">

									<div class="" id="wrapper">

										<div class="readable no-print row">
											<div class="col s12 m12 panel topPanel">

													<!-- Edit / Printable -->
													<div class="switch">
														<label>
																 Edit
																 <input v-model="printable" type="checkbox">
																 <span class="lever"></span>

															 </label>
													</div>

												<?php if(function_exists('wp_ulike')) wp_ulike('get'); ?>
											</div>

										</div>


									<div v-show="!printable" id="base-components" class="boxee">

										<?php if (is_user_logged_in() && $current_user->ID == $post->post_author)  { ?>



											<!-- Dropdown Trigger -->
										  <a class='recipeDropdown-button' href='#' data-activates='dropdownRecipe'><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

										  <!-- Dropdown Structure -->
										  <ul id='dropdownRecipe' class='dropdown-content'>
										    <li><a id="trashPost" href="#!"><i class="fa fa-trash" aria-hidden="true"></i> Delete Recipe</a></li>

										  </ul>



										<?php } ?>



										<div class="row">
											<div class="input-field col s12">
												<input id="recipeName" type="text" class="validate" value="<?php echo $recipeName ?>">
												<label class="active" for="recipeName">Recipe Name</label>
											</div>
											<div class="input-field col s12">
												<textarea id="recipeNotes" class="materialize-textarea"><?php echo $recipeNotes ?></textarea>
												<label class="active" for="recipeNotes">Recipe Notes</label>
											</div>
										</div>

										<div class="row">
											<div class="section-heading">
												<h5 class="left-align">Nicotine Base</h5>
											</div>
											<div class="col s12 m4">
												<div class="input-field">
													<input v-model="nicBaseMg" id="nicBaseMg" type="number" min="0" class="validate" number>
													<label class="active" for="nicBaseMg">Nicotine MG/ML</label>
												</div>
											</div>
											<div class="col s12 m4">
												<div class="input-field">
													<input v-model="nicVG" id="nicBaseVg" @change="changeNicRatio('VG')" type="number" min="0" max="100" class="validate" number>
													<label class="active" for="nicBaseVg">VG ratio %</label>
												</div>
											</div>
											<div class="col s12 m4">
												<div class="input-field">
													<input v-model="nicPG" id="nicBasePg" @change="changeNicRatio('PG')" type="number" min="0" max="100" class="validate" number>
													<label class="active" for="nicBasePg">PG ratio %</label>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="section-heading">
												<h5 class="left-align">Target Measurements</h5>
											</div>
											<div class="col s6">
												<div class="input-field">
													<input id="bottle_ml" type="number" min="1" v-model="targetBottleSize" class="validate" number>
													<label class="active" for="bottle_ml">Bottle Size (ML)</label>
												</div>
											</div>

											<div class="col s6">
												<div class="input-field">
													<input v-model="targetNicMg" id="nic_mgml" type="number" min="0" max="{{nicBaseMg}}" class="" number>
													<label class="active" for="nic_mgml">Nicotine (mg/ml)</label>
												</div>
											</div>

											<div class="col s6">
												<div class="input-field">
													<input v-model="targetRatioVg" id="vg_ratio" type="number" min="0" @change="changeVgPg" max="100" class="vgpgratio validate" number>
													<label class="active" for="vg_ratio">VG Ratio (%)</label>
												</div>
											</div>

											<div class="col s6">
												<div class="input-field">
													<input v-model="targetRatioPg" id="pg_ratio" type="number" min="0" @change="changeVgPg" max="100" class="vgpgratio validate" number>
													<label class="active" for="pg_ratio">PG Ratio (%)</label>
												</div>
											</div>
										</div>

									</div>




									<!-- Ingredients -->
									<ul class="collection boxee" id="recipe-totals" style="padding-top:0;">

										<div v-if="printable" class="collection-item ingredient-header-line">
											<div class="row ingredient-header-line">
												<div class="col s12">
													<h2>{{recTitle}}</h2>
													<!-- <a class="faveIt" href=""><i class="fa-header-o fa" aria-hidden="true">Fave It</i></a> -->
												</div>
											</div>
										</div>


										<li v-if="printable" class="collection-item ingredient-header-line">
											<div class="row ingredient-header-line">
												<div class="col s3">
													<p class="ing-heading" id="targetBottleSize">Bottle Size:<br /> {{targetBottleSize}}&nbsp;ML</p>
													<!-- <span class="parentValue" id="ptargetBottleSize" :class="numClass" v-if="parentRecipe">{{{targetBottleSize - parentRecipe.pbottleml}}}</span> -->
												</div>
												<div class="col s3">
													<p class="ing-heading" id="targetRatioVgPg">VG/PG Ratio:<br /> {{targetRatioVg}}/{{targetRatioPg}}</p>
													<!-- <span class="parentValue" id="ptargetRatioVgPg"></span> -->
												</div>
												<div class="col s6">
													<p class="ing-heading" id="targetNicMg">Target Nicotine:<br /> {{targetNicMg}}&nbsp;MG - <br class="hide-on-med-and-up" /> ({{nicVG}}/{{nicPG}} {{nicBaseMg}}MG Base)</p>
													<!-- <span class="parentValue" id="ptargetNicMg"></span> -->
												</div>
											</div>
										</li>

										<li class="collection-item ingredient-header-line">
											<div class="row ingredient-header-line">
												<div class="col s3">
													<p class="ing-heading">Ingredient</p>
												</div>
												<div class="col s3">
													<p class="ing-heading">drops</p>
												</div>
												<div class="col s3">
													<p class="ing-heading">milliliters</p>
												</div>
												<div class="col s3">
													<p class="ing-heading">grams</p>
												</div>

											</div>
										</li>
										<li class="collection-item" id="vg_line">
											<div class="row ingredient-line">
												<div class="col s3">
													<p class="name-val">VG</p>
												</div>
												<div class="col s3">
													<p class="name-val drops">{{leftoverVgMl | ml_to_drops | roundl 0}}</p>
												</div>
												<div class="col s3">
													<p class="name-val ml">{{leftoverVgMl | roundl}}</p>
												</div>
												<div class="col s3">
													<p class="name-val grams">{{leftoverVgMl | ml_to_grams 'VG' | roundl}}</p>
												</div>

											</div>
										</li>
										<li class="collection-item" id="pg_line">
											<div class="row ingredient-line">
												<div class="col s3">
													<p class="name-val">PG</p>
												</div>
												<div class="col s3">
													<p class="name-val drops">{{leftoverPgMl | ml_to_drops | roundl 0}}</p>
												</div>
												<div class="col s3">
													<p class="name-val ml">{{leftoverPgMl | roundl}}</p>
												</div>
												<div class="col s3">
													<p class="name-val grams">{{leftoverPgMl | ml_to_grams 'PG' | roundl}}</p>
												</div>

											</div>
										</li>
										<li class="collection-item" id="nic_line">
											<div class="row ingredient-line">
												<div class="col s3">
													<p class="name-val">Nicotine</p>
												</div>
												<div class="col s3">
													<p class="name-val drops">{{ nicPercentage | percent_to_ml | ml_to_drops | roundl 0 }}</p>
												</div>
												<div class="col s3">
													<p class="name-val ml">{{ nicPercentage | percent_to_ml | roundl }}</p>
												</div>
												<div class="col s3">
													<p class="name-val grams nic-grams">{{ nicPercentage | percent_to_ml | ml_to_grams "NIC" | roundl }}</p>
												</div>

											</div>
										</li>

										<li v-if="printable" v-for="flavor in flavors" class="collection-item">
											<div class="row ingredient-line">
												<div class="col s3">
													<p class="name-val">{{flavor.name}} - {{flavor.percentage | roundl 4 }}%</p>
												</div>
												<div class="col s3">
													<p class="name-val drops">{{ flavor.percentage | percent_to_ml | ml_to_drops | roundl 0 }}</p>
												</div>
												<div class="col s3">
													<p class="name-val ml">{{ flavor.percentage | percent_to_ml | roundl }}</p>
												</div>
												<div class="col s3">
													<p class="name-val grams">{{ flavor.percentage | percent_to_ml | ml_to_grams flavor.weight_src flavor.custom_weight | roundl }}</p>
												</div>

											</div>
										</li>


									</ul>

									<ul v-show="!printable" class="collapsible" id="flavors" data-collapsible="accordion">
										<li class="header">
											<div class="collapsible-header">
												<div class="row ingredient-header-line">
													<div class="col s3">
														<p class="ing-heading">Flavor Name</p>
													</div>
													<div class="col s3">
														<p class="ing-heading">drops</p>
													</div>
													<div class="col s3">
														<p class="ing-heading">milliliters</p>
													</div>
													<div class="col s3">
														<p class="ing-heading">grams</p>
													</div>

												</div>
											</div>
										</li>

										<!-- v-loop start -->

										<li v-for="flavor in flavors" class="flavor_line">
											<div class="collapsible-header">
												<div class="row ingredient-line">
													<div class="col s3 name">
														<p class="name-val">{{ flavor.name }} - {{flavor.percentage | roundl 4 }}%</p>
													</div>
													<div class="col s3 drops"><p>{{ flavor.percentage | percent_to_ml | ml_to_drops | roundl 0 }}</p></div>
													<div class="col s3 ml"><p>{{ flavor.percentage | percent_to_ml | roundl }}</p></div>
													<!-- <div class="col s3 grams">{{ ml_to_mg( percent_to_ml(flavor.percentage), flavor.weight_src, flavor.custom_weight ) }}</div> -->
													<div class="col s3 grams">
														<p><span class="grams">{{ flavor.percentage | percent_to_ml | ml_to_grams flavor.weight_src flavor.custom_weight | roundl }}</span>
															<i class="material-icons closed right hide-on-small-only">settings</i>
														</p>

													</div>



												</div>
											</div>
											<div class="collapsible-body">
												<div class="ingredient-body row">
													<div class="col s12">

														<div class="input-field col s12 m6">
															<i class="material-icons prefix">textsms</i>
															<label for="flavor-{{flavor.id}}" class="control-label">Flavor Name</label>
															<input class="form-control bs-autocomplete autocomplete-flavors" id="flavor-{{flavor.id}}" v-model="flavor.name" value="{{flavor.name}}" type="text" data-source="" data-hidden_field_id="flavor-id-{{flavor.id}}" data-item_id="id" data-item_label="label" autocomplete="off">
															<input class="form-control hidden-flavor-id" id="flavor-id-{{flavor.id}}" name="flavorid" value="{{flavor.postID}}" type="hidden" readonly>
														</div>


														<div class="input-field col s12 m6">
															<input id="flavor-percent-{{flavor.id}}" v-model="flavor.percentage" type="number" min="0" class="flavor_percentage" number>
															<label for="flavor-percent-{{flavor.id}}" class="active">Percentage</label>
														</div>

														<div class="col s12">
															<a class="waves-effect waves-light btn" @click="removeFlavor(flavor.id)">Delete</a>
															<a class="waves-effect waves-light btn flavor-close right">Close</a>
														</div>
													</div>
												</div>
											</div>
										</li>

									</ul>

									<div v-show="!printable" class="row">
										<div class="col m4 s6">
											<a class="waves-effect waves-light btn" id="addFlavor" @click="addFlavor">Add Flavor</a>
										</div>
									</div>

									<div v-show="printable" class="row boxee">
										<div class="col s12">
											<p><strong>Recipe Notes:</strong></p>
											<div>{{{recNotes}}}</div>
										</div>
									</div>

<!-- This will be used in the future to compare the recipe parent to the post. for now, let's not.  -->
									<?php

									// $postParent = $post->post_parent;
									//
									//
									// if ($postParent != 0) {
									//
									// 	// WP_Query arguments
									// 		$args = array(
									// 			'p'                      => $postParent,
									// 			'post_type'							 => array('recipes'),
									// 		);
									//
									// 		// The Query
									// 		$query = new WP_Query( $args );
									//
									// 	while ($query->have_posts()) : $query->the_post();
									//
									// 		$pTitle = get_the_title();
									// 		$pNicBaseMg = get_field('nicBaseMg');
									// 		$pNicBaseVg = get_field('nicBaseVg');
									// 		$pNicBasePg = get_field('nicBasePg');
									// 		$pBottleMl = get_field('bottle_ml');
									// 		$pNicMgml = get_field('nic_mgml');
									// 		$pVgRatio = get_field('vg_ratio');
									// 		$pPgRatio = get_field('pg_ratio');
									// 		$pVgDrops = get_field('vg_drops');
									// 		$pPgDrops = get_field('pg_drops');
									// 		$pPgMl = get_field('pg_ml');
									// 		$pPgG = get_field('pg_g');
									// 		$pNicDrops = get_field('nic_drops');
									// 		$pNicMl = get_field('nic_ml');
									// 		$pNicG = get_field('nic_g');
									// 		$pVgMl = get_field('vg_ml');

											?>

											<!-- <input class="form-control" id="pnicBaseMg" name="pnicBaseMg" value="<?php echo $pNicBaseMg ?>" type="hidden" readonly>
											<input class="form-control" id="pnicBaseVg" name="pnicBaseVg" value="<?php echo $pNicBaseVg ?>" type="hidden" readonly>
											<input class="form-control" id="pnicBasePg" name="pnicBasePg" value="<?php echo $pNicBasePg ?>" type="hidden" readonly>
											<input class="form-control" id="pbottleml" name="pbottleml" value="<?php echo $pBottleMl ?>" type="hidden" readonly>
											<input class="form-control" id="pnicmgml" name="pnicmgml" value="<?php echo $pNicMgml ?>" type="hidden" readonly>
											<input class="form-control" id="pvgratio" name="pvgratio" value="<?php echo $pVgRatio ?>" type="hidden" readonly>
											<input class="form-control" id="ppgratio" name="ppgratio" value="<?php echo $pPgRatio ?>" type="hidden" readonly>
											<input class="form-control" id="pvgdrops" name="pvgdrops" value="<?php echo $pVgDrops ?>" type="hidden" readonly>
											<input class="form-control" id="ppgdrops" name="ppgdrops" value="<?php echo $pPgDrops ?>" type="hidden" readonly>
											<input class="form-control" id="ppgml" name="ppgml" value="<?php echo $pPgMl ?>" type="hidden" readonly>
											<input class="form-control" id="ppgg" name="ppgg" value="<?php echo $pPgG ?>" type="hidden" readonly>
											<input class="form-control" id="pnicdrops" name="pnicdrops" value="<?php echo $pNicDrops ?>" type="hidden" readonly>
											<input class="form-control" id="pnicml" name="pnicml" value="<?php echo $pNicMl ?>" type="hidden" readonly>
											<input class="form-control" id="pnicg" name="pnicg" value="<?php echo $pNicG ?>" type="hidden" readonly>
											<input class="form-control" id="pvgml" name="pvgml" value="<?php echo $pVgMl ?>" type="hidden" readonly> -->



											<style media="screen">
											p.name-val:after,
											p.ing-heading:after {
												font-size: 10px;
												line-height: 1;
												margin-left: 10px;
											}
											#targetBottleSize:after {
													/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";
												}*/
											#targetRatioVgPg:after {
													/*content: "{{targetRatioVg - parentRecipe.pvgratio}} / {{{targetRatioPg - parentRecipe.ppgratio}}}";*/
												}
											#targetNicMg:after {
													/*content: "{{targetNicMg - }}";*/
												}
											#vg_line p.name-val.drops:after {
													/*content: "{{{parentRecipe.pvgml | posOrNegative }}}";*/
												}
											#vg_line p.name-val.ml:after {
												/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
											}
											#vg_line p.name-val.grams:after {
												/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
											}
											#pg_line p.name-val.drops:after {
													/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
												}
											#pg_line p.name-val.ml:after {
												/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
											}
											#pg_line p.name-val.grams:after {
												/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
											}
											#nic_line p.name-val.drops:after {
													/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
												}
											#nic_line p.name-val.ml:after {
												/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
											}
											#nic_line p.name-val.grams:after {
												/*content: "{{{nicBaseMg - parentRecipe.pnicBaseMg}}}";*/
											}
											</style>


									<?php
									// 	endwhile;
									// 	wp_reset_postdata();
									//
									// }
									?>






									<!-- // Ingredients -->

									<?php


								// check if current user is the post author
								global $current_user;
								get_currentuserinfo();

								if (is_user_logged_in() && $current_user->ID == $post->post_author)  { ?>

									<div v-show="!printable" class="row center-align">
											<a class="waves-effect waves-light btn" id="updateRecipe">Update Recipe</a>
									</div>

							<?php
						} elseif (is_user_logged_in()) { ?>
							<div v-show="!printable" class="row center-align">
									<a class="waves-effect waves-light btn" id="submitRecipe">Create Fork</a>
							</div>
						<?php
						}
						?>


								</div>

							</div>

<?php
// get posts
$forks = get_posts(array(
	'post_type'			=> 'recipes',
	'posts_per_page'	=> -1,
	'post_parent'			=> get_the_ID(),
	// 'meta_key'			=> 'start_date',
	// 'orderby'			=> 'meta_value',
	// 'order'				=> 'DESC'
));
 ?>


							<div class="row">
	 <div class="col s12">
		 <ul class="tabs">
			 <li class="tab col s3"><a class="active" href="#comments-tab"><i class="fa fa-comments" aria-hidden="true"></i> <?php echo get_comments_number() ?> Comments</a></li>
				<?php if($forks): ?>
					<li class="tab col s3"><a class="" href="#forks-tab"><i class="fa fa-code-fork" aria-hidden="true"></i>
					<?php $post_count = 0; ?>
				  <?php foreach( $forks as $fork ):

	 					setup_postdata( $fork );
						$post_count++;
						endforeach;

					 	echo $post_count; ?> Forks

					<?php wp_reset_postdata(); ?>
					</a></li>
				<?php endif; ?>

		 </ul>
	 </div>
	 <div id="comments-tab" class="col s12">
		 <div class="boxee">
 			<?php


 			// the_post_navigation();

 			//If comments are open or we have at least one comment, load up the comment template.
 			if ( comments_open() || get_comments_number() ) :
 				comments_template();
 			endif;


 		?>
 		</div>
	 </div>
	 <div id="forks-tab" class="col s12">


			<?php



			if( $forks ): ?>
			<div id="revisions" class="boxee">
  			<h4>Forks</h4>
				<ul>
				<?php foreach( $forks as $fork ):
					setup_postdata( $fork )
					?>
					<li>
						<div class="card recipe-card">
	            <div class="card-content">

	              <span class="card-title grey-text text-darken-4"><a href="<?php echo $fork->guid; ?>"><?php echo $fork->post_name; ?></a></span>
	                <?php
	                //  echo $likes;
	                ?>
	                <p class="card-subtitle grey-text text-darken-2"><a href="<?php echo um_user_profile_url() ?>"><?php um_fetch_user($fork->post_author); echo um_user('user_login'); ?></a></p>

	              <p class="card-subtitle grey-text text-darken-2"><?php echo $fork->post_content_filtered; ?></p>

	            </div>

							<div class="status-bar">
	              <?php if(function_exists('wp_ulike')) wp_ulike('get'); ?>
	            <div class="comment-box">
	            <div class="comment-box-content">
	              <a href="<?php echo $fork->guid.'#comments-tab' ?>">
	            <i class="fa fa-comments" aria-hidden="true"></i>
	            <?php echo get_post_field( 'comment_count', $fork->ID ) ?>
	          </a>
	            </div>
	            </div>
	            <div class="fork-box">
	              <div class="fork-box-content">
	                <a href="<?php echo $fork->guid.'#forks-tab' ?>">
	                <i class="fa fa-code-fork" aria-hidden="true"></i>
	            <span><?php echo count(get_children($fork->ID)) ?></span>
	          </a>
	              </div>
	            </div>
	            </div>

	            <div class="card-action">
	              <div class="col-left">
	                <?php while ( have_rows('recipeFlavors', $fork->ID) ) : the_row(); ?>
	                      <?php $flavor_object = get_sub_field('recipeFlavor', $fork->ID); ?>
	                      <?php if( $flavor_object ): ?>
	                          <?php $flavor = $flavor_object; setup_postdata( $flavor ); ?>

	                           <?php $post_slug = $flavor->post_name;
														 $flavor_url = $flavor->guid; ?>
	                          <a href="<?php echo $flavor_url; ?>" class="chip <?php echo $post_slug ?>"><?php echo $post_slug; ?></a>
	                          <?php wp_reset_postdata(); ?>
	                      <?php endif; ?>
	                <?php endwhile; ?>
	              </div>
	              <div class="col-right">

	              </div>



	            </div>
	          </div>
					</li>
				<?php endforeach; ?>
				</ul>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

	 </div>
 </div>

										<script type="text/javascript">
										$(document).ready(function(){
									    $('ul.tabs').tabs();
									  });

										</script>


							</div>
						</div>

	<?php endwhile; // End of the loop. ?>

				  </div>





				<footer class="entry-footer">
					<?php juicesauce_entry_footer(); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->


			<?php


			if (is_user_logged_in() && $current_user->ID == $post->post_author)  {

				//Here is the start of the magic code aka how I pick up girls "is yo daddy a baker cuz you got buns hun"
			//get the motherfucking user ID
			$user = wp_get_current_user();
			$userID = $user->ID;
			//so here you usually close it down to some roles but we going all out here so fuck this you already have it that only signed up people can see this anyways
			// $allowed_roles = array('editor', 'administrator', 'author');
			// if( array_intersect($allowed_roles, $user->roles ) ) {
			//lets get this mother fucking scrip going ya?
			?>


			<script>
			$(document).ready(function () {

				$(".lever").click(function(){
					Materialize.updateTextFields();
				});

				$( "#trashPost" ).click(function( e ) {
						e.preventDefault();
						var recipeID = <?php echo get_the_ID(); ?>;

					var r = confirm("You sure you wanna delete this recipe?");
					if (r == true) {
						//Then we get the WordPress Ajax URL
						var ajaxurl = 'http://'+window.location.host+'/wp-admin/admin-ajax.php';
						// this mother fucker will help up send all the data and use a function in function.php to make a post
						    //Ajax thing
						    $.ajax({
						                    //we just doing normal ajax from wordpress and an action meaning a function in functions.php
						                    url: ajaxurl + "?action=trashPost",
						                    // the last one is userID we need that so we can create the post and assign this as the author
						                    data: {recipeID: recipeID},
						                    //type of request, usually post
						                    type: 'POST',
						                    success: function(data) {

																	window.location.href = '/';
						                    },
						                    error: function(xhr, ajaxOptions, thrownerror) { }
						            }); //ajax function
					} else {
					}

				});


			//first we detect when the form gets submited
			$( "#updateRecipe" ).click(function( e ) {
			//we prevent from doing the default aka refreshing the page and send it via post to w/e page
			e.preventDefault();

			// Check if values are filled out
			if ($.trim($("#recipeName, #nicBaseMg, #nicBaseVg, #nicBasePg, #bottle_ml, #nic_mgml, #vg_ratio, #pg_ratio").val()) === "") {

					alert('you did not fill out one of the fields!');
			} else {

			// GET THE RECIPE ID
			var recipeID = <?php echo get_the_ID(); ?>;
			//then we get the values from the form fields
			var recipeName = $("#recipeName").val();
			var recipeNotes = $("#recipeNotes").val();
			//since I am a bit fucking drunk I am going to do 2 you can do the rest, I believe in you bruh

			// Custom Fields
			var nicBaseMg = $('#nicBaseMg').val();
			var nicBaseVg = $('#nicBaseVg').val();
			var nicBasePg = $('#nicBasePg').val();
			var bottle_ml = $('#bottle_ml').val();
			var nic_mgml = $('#nic_mgml').val();
			var vg_ratio = $('#vg_ratio').val();
			var pg_ratio = $('#pg_ratio').val();
			var vg_drops = $('#vg_line p.name-val.drops').text();
			var vg_ml = $('#vg_line p.name-val.ml').text();
			var vg_g = $('#vg_line p.name-val.grams').text();
			var pg_drops = $('#pg_line p.name-val.drops').text();
			var pg_ml = $('#pg_line p.name-val.ml').text();
			var pg_g = $('#pg_line p.name-val.grams').text();
			var nic_drops = $('#nic_line p.name-val.drops').text();
			var nic_ml = $('#nic_line p.name-val.ml').text();
			var nic_g = $('#nic_line p.name-val.grams').text();

			// Repeater
			var recipeFlavors = [];

			// New loop to get all info for each flavor instead of just the flavor
				$( ".flavor_line" ).each(function( index ) {
					var fDrops = $(this).find(".drops p").text();
					var fMl = $(this).find(".ml p").text();
					var fG = $(this).find(".grams .grams").text();
					var rId = $(this).find("input.hidden-flavor-id").val();
					var flavor_perc = $(this).find(".flavor_percentage").val();

					// Push recipeFlavor object onto recipeFlavors
					recipeFlavors.push({recipeFlavor:{id: rId},flavor_drops: fDrops,flavor_ml: fMl,flavor_g: fG, flavor_perc: flavor_perc});
				});


			//Then we get the WordPress Ajax URL
			var ajaxurl = 'http://'+window.location.host+'/wp-admin/admin-ajax.php';
			// this mother fucker will help up send all the data and use a function in function.php to make a post
				//Ajax thing
				$.ajax({
					//we just doing normal ajax from wordpress and an action meaning a function in functions.php
					url: ajaxurl + "?action=updateJuice",
					//here is the info we should be sending aka content  and crazy advanced custom fields
					//so in the example below is pretty easy recipeName: is the name of the post and the next variable is the date so is  namePost : data
					/**
					 *  Example is recipeName : recipeName - the 1st recipeName is the variable that you are passing to functions.php and the next one is the variable that you
					 * made above getting the data from JQuery so each time you want to add an extra variable/form field you get the value of it via jquery and then send it to
					 * functions.php via ajax in the data array
					 *  also you could add all this into one array and just foreach on functions.php if you send all this as a big json object
					 * */
					// the last one is userID we need that so we can create the post and assign this as the author
					data: {recipeID: recipeID, recipeName: recipeName, recipeNotes: recipeNotes, userID: <?php echo $userID; ?>, fields: {nicBaseMg: nicBaseMg, nicBaseVg: nicBaseVg, nicBasePg: nicBasePg, bottle_ml: bottle_ml, nic_mgml: nic_mgml, vg_ratio: vg_ratio, pg_ratio: pg_ratio, vg_drops: vg_drops, vg_ml: vg_ml, vg_g: vg_g, pg_drops: pg_drops, pg_ml: pg_ml, pg_g: pg_g, nic_drops: nic_drops, nic_ml: nic_ml, nic_g: nic_g, recipeFlavors: recipeFlavors},},
					//type of request, usually post
					type: 'POST',
					success: function(data) {
					// window.location.href = '/' + data;
					window.location.href = data;
					console.log(data);
					},
					error: function(xhr, ajaxOptions, thrownerror) { }
					}); //ajax function
				}
			}); //submit function


			}); //doc ready
			//donezo cya in functions.php

			</script>

			<?php } ?>
			<?php
			$user = wp_get_current_user();
			$userID = $user->ID;
			 ?>

			<script type="text/javascript">
				$(document).ready(function () {

					//Fill hidden fields (flavors) with the value of the flavor id on page load.


					//first we detect when the form gets submited
					$( "#submitRecipe" ).click(function( e ) {

					//we prevent from doing the default aka refreshing the page and send it via post to w/e page
					  e.preventDefault();
					//then we get the values from the form fields
					var recipeName = $("#recipeName").val();
					var recipeNotes = $("#recipeNotes").val();
					var recipeID = <?php echo get_the_ID(); ?>;
					//since I am a bit fucking drunk I am going to do 2 you can do the rest, I believe in you bruh

					// Custom Fields
					  var nicBaseMg = $('#nicBaseMg').val();
					  var nicBaseVg = $('#nicBaseVg').val();
					  var nicBasePg = $('#nicBasePg').val();
					  var bottle_ml = $('#bottle_ml').val();
					  var nic_mgml = $('#nic_mgml').val();
					  var vg_ratio = $('#vg_ratio').val();
					  var pg_ratio = $('#pg_ratio').val();
					  var vg_drops = $('#vg_line p.name-val.drops').text();
					  var vg_ml = $('#vg_line p.name-val.ml').text();
					  var vg_g = $('#vg_line p.name-val.grams').text();
					  var pg_drops = $('#pg_line p.name-val.drops').text();
					  var pg_ml = $('#pg_line p.name-val.ml').text();
					  var pg_g = $('#pg_line p.name-val.grams').text();
					  var nic_drops = $('#nic_line p.name-val.drops').text();
					  var nic_ml = $('#nic_line p.name-val.ml').text();
					  var nic_g = $('#nic_line p.name-val.grams').text();

						// rest post parent to original recipe, so grabbing the post parent of the current recipe

					  // Repeater
					  var recipeFlavors = [];

					  // New loop to get all info for each flavor instead of just the flavor
					    $( ".flavor_line" ).each(function( index ) {
					      fDrops = $(this).find(".drops p").text();
					      fMl = $(this).find(".ml p").text();
					      fG = $(this).find(".grams .grams").text();
					      rId = $(this).find("input.hidden-flavor-id").val();
					      flavor_perc = $(this).find(".flavor_percentage").val();

					      // Push recipeFlavor object onto recipeFlavors
					      recipeFlavors.push({recipeFlavor:{id: rId},flavor_drops: fDrops,flavor_ml: fMl,flavor_g: fG, flavor_perc: flavor_perc});
					    });


					//Then we get the WordPress Ajax URL
					var ajaxurl = 'http://'+window.location.host+'/wp-admin/admin-ajax.php';
					// this mother fucker will help up send all the data and use a function in function.php to make a post
					    //Ajax thing
					    $.ajax({
					                    //we just doing normal ajax from wordpress and an action meaning a function in functions.php
					                    url: ajaxurl + "?action=makeJuice",
					                    //here is the info we should be sending aka content  and crazy advanced custom fields
					                    //so in the example below is pretty easy recipeName: is the name of the post and the next variable is the date so is  namePost : data
					                    /**
					                     *  Example is recipeName : recipeName - the 1st recipeName is the variable that you are passing to functions.php and the next one is the variable that you
					                     * made above getting the data from JQuery so each time you want to add an extra variable/form field you get the value of it via jquery and then send it to
					                     * functions.php via ajax in the data array
					                     *  also you could add all this into one array and just foreach on functions.php if you send all this as a big json object
					                     * */
					                    // the last one is userID we need that so we can create the post and assign this as the author
					                    data: {recipeName: recipeName, recipeNotes: recipeNotes, userID: <?php echo $userID; ?>, fields: {original_recipe: <?php echo get_the_ID(); ?>, nicBaseMg: nicBaseMg, nicBaseVg: nicBaseVg, nicBasePg: nicBasePg, bottle_ml: bottle_ml, nic_mgml: nic_mgml, vg_ratio: vg_ratio, pg_ratio: pg_ratio, vg_drops: vg_drops, vg_ml: vg_ml, vg_g: vg_g, pg_drops: pg_drops, pg_ml: pg_ml, pg_g: pg_g, nic_drops: nic_drops, nic_ml: nic_ml, nic_g: nic_g, recipeFlavors: recipeFlavors},},
					                    //type of request, usually post
					                    type: 'POST',
					                    success: function(data) {
					                    window.location.href = data;
					                    console.log(data);
					                    },
					                    error: function(xhr, ajaxOptions, thrownerror) { }
					            }); //ajax function
					    }); //submit function
				});
			</script>



		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_sidebar();
get_footer();

?>
